<?php namespace Valis\Unimall;

interface Unimall
{

    public function add();

    public function detail();

    public function config();

    public function templates();

    public function banners();

}
