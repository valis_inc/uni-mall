<?php

namespace Valis\Unimall;

use Illuminate\Support\ServiceProvider;
// use Valis\Unimall\RakutenRws_Client as Client;

require __DIR__ . '/RakutenSDK/autoload.php';

class UnimallServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('valis/unimall');

		// Laravel用クラスローダーの登録
		// \ClassLoader::addDirectories( array(
		// 	__DIR__.'/../../Commands',
		// 	__DIR__.'/../../Controllers',
		// 	__DIR__.'/../../Models',
		// 	__DIR__.'/../../Seeds',
		// ) );

		// ビュー、言語、設定の名前空間（プレフィックス）設定
		// \View::addNamespace( 'Valis\Unimall', __DIR__.'/../../Views' );
		// \Lang::addNamespace( 'Valis\Unimall', __DIR__.'/../../Languages' );
		// \Config::addNamespace( 'Valis\Unimall', __DIR__.'/../../Configurations' );

		// ルート定義の読み込み
		// require __DIR__.'/../../routes.php';

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// CO Interface化 150606
		// $this->app->bind('Unimall',function()
		// {
		// 	return new Unimall;
		// 	// return new \Valis\Unimall\Unimall;
		// });
		$this->app->bind('Rakuten',function()
		{
			return new Rakuten;
		});
		$this->app->bind('Item',function()
		{
			return new Item;
		});
		$this->app->bind('Client', function(){
			return new \RakutenRws_Client;
		});

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('Unimall');
	}

}
