<?php namespace Valis\Unimall;

use Illuminate\Support\Facades\App;

class Rakuten implements Unimall {

	const HOST = 'ftp.rakuten.ne.jp';
	// const HOST = 'c5666.hosting.ramzii.com';//<-testatramzii
	const PORT = '16910';
	// const PORT = '21';//<-testatramzii
	const DIR  = '/uni';
	// const DIR  = '/web/uni';//<-testatramzii

	protected $item;
	// protected $store = 'rakuten';
	protected $SDK;

	// Option Obj
	protected $OP;

	public $genre = array(
		"CD・DVD・楽器" => "CD・DVD・楽器",
	    "インテリア・寝具・収納" => "インテリア・寝具・収納",
	    "おもちゃ・ホビー・ゲーム" => "おもちゃ・ホビー・ゲーム",
	    "キッズ・ベビー・マタニティ" => "キッズ・ベビー・マタニティ",
	    "日用品雑貨・文房具・手芸" => "日用品雑貨・文房具・手芸",
	    "ジュエリー・アクセサリー" => "ジュエリー・アクセサリー",
	    "スポーツ・アウトドア" => "スポーツ・アウトドア",
	    "ダイエット・健康" => "ダイエット・健康",
	    "水・ソフトドリンク" => "水・ソフトドリンク",
	    "パソコン・周辺機器" => "パソコン・周辺機器",
	    "バッグ・小物・ブランド雑貨" => "バッグ・小物・ブランド雑貨",
	    "レディースファッション" => "レディースファッション",
	    "花・ガーデン・DIY" => "花・ガーデン・DIY",
	    "ペット・ペットグッズ" => "ペット・ペットグッズ",
	    "TV・オーディオ・カメラ" => "TV・オーディオ・カメラ",
	    "車・バイク" => "車・バイク",
	    "食品" => "食品",
	    "美容・コスメ・香水" => "美容・コスメ・香水",
	    "本・雑誌・コミック" => "本・雑誌・コミック",
	    "旅行・出張・チケット" => "旅行・出張・チケット",
	    "学び・サービス・保険" => "学び・サービス・保険",
	    "百貨店・総合通販・ギフト" => "百貨店・総合通販・ギフト",
	    "デジタルコンテンツ" => "デジタルコンテンツ",
	    "車用品・バイク用品" => "車用品・バイク用品",
	    "インナー・下着・ナイトウエア" => "インナー・下着・ナイトウエア",
	    "日本酒・焼酎" => "日本酒・焼酎",
	    "ビール・洋酒" => "ビール・洋酒",
	    "スイーツ・お菓子" => "スイーツ・お菓子",
	    "医薬品・コンタクト・介護" => "医薬品・コンタクト・介護",
	    "メンズファッション" => "メンズファッション",
	    "靴" => "靴",
	    "腕時計" => "腕時計",
	    "キッチン用品・食器・調理器具" => "キッチン用品・食器・調理器具",
	    "家電" => "家電",
	    "スマートフォン・タブレット" => "スマートフォン・タブレット",
	);

	public function __construct()
	{
		$this->item = App::make('Item');
		$this->SDK = App::make('Client');
		// アプリID (デベロッパーID) をセットします
		// Mod オリジナル環境変数から取得
		$rakuten_id = \Config::get('const.MY_RAKUTEN_ID');
		$this->SDK->setApplicationId($rakuten_id);
		$affi_id = \Config::get('const.MY_RAKUTEN_AFFI');
		$this->SDK->setAffiliateId($affi_id);
	}

	public function add()
	{

	}

	public function detail()
	{

	}

	public function config()
	{

	}

	public function templates()
	{

	}

	public function banners()
	{

	}

	public function show()
	{
		// IchibaItem/Search API から、keyword=うどん を検索します
		$response = $this->SDK->execute('IchibaItemSearch', array(
  			'shopCode' => 'kyounokura'
		));
		// レスポンスが正しいかを isOk() で確認することができます
		if ($response->isOk()) {
			// 配列アクセスによりレスポンスにアクセスすることができます。
			// print "<pre>";var_dump($response);print "<pre>";exit;
			return $response;
		} else {
			return 'Error:'.$response->getMessage();
		}
		// return $this->item->show();
	}

	/**
	 * Settings and GET Items from API
	 * @version 1.0
	 * @note
	 * @param	(str) $shopCode	店舗アカウント名
	 * @param	(str) $page		取得ページ（最大ページ10=config.MAX_PAGE）
	 * @param	(str) $option	オプション
	 * @param	(str) $area		翻訳する国名
	 * @return	(arr) $items	商品情報 or NULL or false
	 */
	public function searchShopCode($shopCode, $page=1, $option='defaults', $area='NONE')
	{
		$this->OP = App::make('stdClass');
		$this->OP->defaults = [
			'shopCode'		 => $shopCode,
			'availability'	 => 1,
			'imageFlag'		 => 1,
			'page'			 => $page,
		];
		$this->OP->global = [
			'US' =>[
				'shipOverseasFlag' => 1,
				'shipOverseasArea' => ['US', 'ALL'],
				'orFlag' => 1,
			],
			'CN' =>[
				'shipOverseasFlag' => 1,
				'shipOverseasArea' => ['CN', 'ALL'],
				'orFlag' => 1,
			],
			'KR' =>[
				'shipOverseasFlag' => 1,
				'shipOverseasArea' => ['KR', 'ALL'],
				'orFlag' => 1,
			],
			'NONE' =>[],
		];
		$this->OP->asuraku = [
			'asurakuFlag' => 1,
			'asurakuArea' => 0,
		];
		$this->OP->review = [
			'hasReviewFlag' => 1,
			'sort' => '+reviewCount',
		];
		$this->OP->point = [
			'pointRateFlag' => 1,
			'pointRate' => 2,
		];

		$options = array_merge($this->OP->defaults, $this->OP->$option);
		$options = array_merge($options, $this->OP->global[$area]);
		$response = $this->SDK->execute('IchibaItemSearch', $options);

		// レスポンスが正しいかを isOk() で確認することができます
		if ($response->isOk()) {
			// 配列アクセスによりレスポンスにアクセスすることができます。
			foreach ($response as $item)
			{
				$items[] = $item;
			}
			return (!empty($items)) ? $items : null ;
		} else {
			// return 'Error:'.$response->getMessage();
			return false;
		}
	}

	public function ftpConnect($ftp, $putFiles=false)
	{
		// 接続
		$conn_id = ftp_connect(self::HOST, self::PORT) or die( 'サーバーに接続できませんでした' );
\Log::debug($ftp['user']. '@' .$ftp['password']);
		// ログイン
		if (@ftp_login($conn_id, $ftp['user'], $ftp['password']))
		{
\Log::debug('<<ログイン成功>>');
// ftp_chdir($conn_id, 'web');//ramziiテスト専用
// \Log::debug('<<cd web>>');
			// ディレクトリ作成
			if (@ftp_mkdir($conn_id, self::DIR))
			{
				// Passiveモードオン
				ftp_pasv($conn_id, true);
\Log::debug('<<uni作成成功>>');
				ftp_chdir($conn_id, self::DIR);
				// 暗号化キーファイルを書き込み
				file_put_contents('./key.txt', $ftp['unique_key'], LOCK_EX);
				$file = './key.txt';
				$fp = fopen($file, 'r');
\Log::debug($fp);
				// キーファイルをput
				if (ftp_fput($conn_id, $file, $fp, FTP_ASCII)) {
\Log::debug('<<ファイルput成功>>');
					$result['flg'] = true;
				} else {
\Log::debug('ファイルput失敗');
					$result['flg'] = false;
					$result['msg'] = '初期ファイルの保存に失敗しました';
				}
			}
			else
			{
				// todo::ファイル読み込み認証
\Log::debug('<<ディレクトリ存在確認>>');
				ftp_pasv($conn_id, true);
				ftp_chdir($conn_id, self::DIR);
				$key = file_get_contents('./key.txt');
\Log::info('key is...'. $key);
\Log::info(\Hash::make($ftp['user']));
\Log::info(\Hash::check($ftp['unique_key'], $key));
				if ( \Hash::check($ftp['user'], $key) )
				{
					$result['flg'] = true;
					$result['msg'] = '認証が取れました';
\Log::info($result['msg']);

					// IMPORTANT::PutsDirectoryPart
					if ( $putFiles == true )
					{
						// data.json 作成
						$json = '{ "id": "'. $ftp['id'] .'", "css": "'. $ftp['css'] .'"}';
						file_put_contents('./data.json', $json, LOCK_EX);
						$file = './data.json';
						$fp = fopen($file, 'r');
						if (ftp_fput($conn_id, $file, $fp, FTP_ASCII)) {
\Log::debug('jsonファイルput成功');
\Log::info('css is...'. $ftp['css']);
							$result = self::ftpPut($ftp['user'], $ftp['password']);
							return $result;

						} else {
\Log::debug('jsonファイルput失敗');
							$result['flg'] = false;
							$result['msg'] = 'jsonファイルの保存に失敗しました';
							return $result;
						}
					}

					return $result;
				}
				else
				{
					$result['flg'] = false;
					$result['msg'] = '認証できませんでした';
					return $result;
				}
			}
		}
		else
		{
\Log::debug('ログインに失敗しました');
			$result['flg'] = false;
			$result['msg'] = 'ログインに失敗しました';
		}
		ftp_close($conn_id);
		if( isset($fp) )fclose($fp);

		return $result;
	}

	public function ftpPut($user, $pass)
	{
		/**
		* CAUTION::
		* MakingTemplatesDirectory Function
		* attention to Rewrite
		*/
		// $host = 'ftp.rakuten.ne.jp';
		// $port = '16910';
		// $id = 'kyounokura';
		// $pass = '11Komaki';
		// lftp ftp.rakuten.ne.jp -p 16910 -u kyounokura,11Komaki
		// $command = 'mkdir uni;cd uni;mkdir ja;mkdir en;';

		/* ver.2.0 */
		// $remote_dir = 'uni';
		$esc_user = escapeshellarg($user);
		$esc_pass = escapeshellarg($pass);
\Log::info($esc_user);
\Log::info($esc_pass);
		$local_dir_path = public_path() . '/templates/rakuten/*';
		$command = "ncftpput -R -dz -u $esc_user -P ".self::PORT." -p $esc_pass ".self::HOST." ".self::DIR." ".$local_dir_path ."";
\Log::info($command);
		// exec("ncftpput -R -m -u $id -P $port -p $pass $host $remote_dir $local_dir_path");
		// if( exec("ncftpput -R -z -u $esc_user -P ".self::PORT." -p $esc_pass ".self::HOST." ".self::DIR." $local_dir_path", $log) )
		// if( exec($command, $log) )
		$rtn = [];
		$flg = null;
		exec($command, $rtn, $flg);
		if( $flg == 0 )
		{
\Log::info('Success!MakingNewStore!!');
\Log::info($rtn);
\Log::info($flg);
			$result['msg'] = '楽天店舗が作成されました！';
			$result['flg'] = true;
			return $result;
		}
		else
		{
\Log::info('Failed...somethingWrong...');
\Log::info($rtn);
\Log::info($flg);
			$result['msg'] = '店舗を作成できませんでした。';
			$result['flg'] = false;
			return $result;
		}

/* ver.1.0
\Log::info('接続前');
		$conn = ftp_connect($host, $port) or die(
			\Log::info($host.'へ接続できませんでした。')
			// return Redirect::to('RakutenController@index')->with('err', '接続に失敗しました');
			);
\Log::info('接続後');
		if (@ftp_login($conn, $id, $pass))
		{
			\Log::info("接続中");
			@ftp_mkdir($conn, 'uni');
			ftp_chdir($conn, 'uni');
			if (@ftp_put($conn, $remote, $local, FTP_BINARY))
			{
				\Log::info('ファイルプット');
			}
		}
		else {
			\Log::info("接続できませんでした。");
			return \Redirect::to('RakutenController@getIndex')->with('err', '接続に失敗しました');
		}
\Log::info('ログインテスト');
		ftp_close($conn);

		return \Redirect::to('RakutenController@getIndex')->with('success', '成功');


		// exec( 'lftp '. $host .' -p '. $port . ' -u ' . $id . '[,'. $pass .']' );

*/
	}


/**
 * CURL Login Module
 *
 */
public function curlModule($_URL)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_URL);
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	$meta = stream_get_meta_data($fp = tmpfile());
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	$output = curl_exec($ch) or die('error ' . curl_error($ch));
	curl_close($ch);

	return $output;
}


	public function curlLogin()
	{
		// TODO::path where cookie?
	    //RMSログインページへ移動(初回)
	    $URL1of3 = "https://glogin.rms.rakuten.co.jp/";
		// $URL1of3 = "133.237.16.72";
		$log = fopen('/vagrant/uni-mall/curl_log.txt', 'a');
		$fp = fopen("tmp", "w");
/**
 * 1ページ目
 * @page RMSログイン
 * @note cookie取得 ReadOnly
 */
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $URL1of3);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		// $meta = stream_get_meta_data($fp = tmpfile());
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	    $output = curl_exec($ch) or die('error ' . curl_error($ch));
	    curl_close($ch);
		//hidden取得＆パラメータをセット
	    $params1of3 = array(
	        "login_id" => 'komaki3433', //for test
	        "passwd" => 'komakisaikou1', //for test
			// "module" => 'BizAuth',
			// "action" => 'BizAuthCustomerAttest',
			// "sp_id" => '1',
	        "submit"  => "楽天会員の認証へ"
	    );
		preg_match_all("/\<input.*type=\"hidden\".*\>/", $output, $inputs);
		array_walk($inputs[0], function($data)use(&$params1of3){
			preg_match("/name\=\"(.*?)\"/", $data, $name);
			preg_match("/value\=\"(.*?)\"/", $data, $value);
			if ( !empty($name[1]) && !empty($value[1]) )
			{
				$params1of3 += array($name[1] => $value[1]);
			}
			return $params1of3;
		});
\Log::info('1page,CLEAR!!');
echo "-1------------------------------<br>";
echo $output;
echo "-------------------------------<br>";
/**
 * 2ページ目
 * @page RMSログイン
 * @note cookie取得 Read|Post
 */
		//R-login認証
	    $ch = curl_init();
		$meta = stream_get_meta_data($fp = tmpfile());
	    curl_setopt($ch, CURLOPT_URL, $URL1of3);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_HEADER, TRUE);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
	    curl_setopt($ch, CURLOPT_POST, TRUE);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $params1of3);
	    $output = curl_exec($ch) or die('error ' . curl_error($ch));
file_put_contents('/vagrant/uni-mall/page2.html', $output);
	    curl_close($ch);
		//hidden取得＆パラメータをセット
		$URL2of3 = "https://glogin.rms.rakuten.co.jp/";
		$params2of3 = array(
			"user_id" => 'wasabi_dev', //for test
			"user_passwd" => 'wasabi2012', //for test
			"submit"  => "ログイン"
		);
		preg_match_all("/\<input.*type=\"hidden\".*\>/", $output, $inputs);
		// is_array?
		array_walk($inputs[0], function($data)use(&$params2of3){
			preg_match("/name\=\"(.*?)\"/", $data, $name);
			preg_match("/value\=\"(.*?)\"/", $data, $value);
			if ( !empty($name[1]) && !empty($value[1]) )
			{
				$params2of3 += array($name[1] => $value[1]);
			}
			return $params2of3;
		});
\Log::info('2page,CLEAR!!');
echo $output;
echo "--2-----------------------------<br>";
/**
 * 3ページ目
 * @page 楽天ログイン
 * @note cookie取得 Read|Post
 */
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL2of3);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params2of3);
		$output = curl_exec($ch) or die('error ' . curl_error($ch));
		curl_close($ch);
		// $URL3of3 = "https://layout.rms.rakuten.co.jp/rms/mall/rsf/layout/vc?__event=RL50_001_000&shop_bid=275383";
		$URL3of3 = 'https://glogin.rms.rakuten.co.jp/';
		$params3of3 = array(
			'submit' => '　　　次へ　　　'
		);
		preg_match_all("/\<input.*type=\"hidden\".*\>/", $output, $inputs);
		// is_array?
		array_walk($inputs[0], function($data)use(&$params3of3){
			preg_match("/name\=\"(.*?)\"/", $data, $name);
			preg_match("/value\=\"(.*?)\"/", $data, $value);
			if ( !empty($name[1]) && !empty($value[1]) )
			{
				$params3of3 += array($name[1] => $value[1]);
			}
			return $params3of3;
		});
file_put_contents('/vagrant/uni-mall/page3.html', $output);
\Log::info('3page,CLEAR!!');
echo $output;
echo "---3----------------------------<br>";
/**
 * 4ページ目
 * @page ログイン後バナー
 * @note cookie取得 Read|Post
 */
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL3of3);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params3of3);
		$output = curl_exec($ch) or die('error ' . curl_error($ch));
		curl_close($ch);
		$URL4of4 = 'https://mainmenu.rms.rakuten.co.jp/';
		$params4of4 = array(
			'submit' => '上記を遵守していることを確認の上、RMSを利用します'
		);
		preg_match_all("/\<input.*type=\"hidden\".*\>/", $output, $inputs);
		// is_array?
		array_walk($inputs[0], function($data)use(&$params4of4){
			preg_match("/name\=\"(.*?)\"/", $data, $name);
			preg_match("/value\=\"(.*?)\"/", $data, $value);
			if ( !empty($name[1]) && !empty($value[1]) )
			{
				$params4of4 += array($name[1] => $value[1]);
			}
\Log::info($params4of4);
			return $params4of4;
		});
file_put_contents('/vagrant/uni-mall/page4.html', $output);
\Log::info('4page,CLEAR!!');
echo $output;
echo "-----4--------------------------<br>";
/**
 * 5ページ目
 * @page ログイン後バナー
 * @note cookie取得 Read|Post
 */
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL4of4);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params4of4);
		$output = curl_exec($ch) or die('error ' . curl_error($ch));
		curl_close($ch);
\Log::info($params4of4);
echo $output;
echo "-----4-1-------------------------<br>";
// exit;
// 		$URL5of5 = 'https://mainmenu.rms.rakuten.co.jp/#contents';
// 		$ch = curl_init();
// 		curl_setopt($ch, CURLOPT_URL, $URL5of5);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// 		curl_setopt($ch, CURLOPT_HEADER, TRUE);
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
// 		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
// 		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
// 		// curl_setopt($ch, CURLOPT_POST, TRUE);
// 		// curl_setopt($ch, CURLOPT_POSTFIELDS, $params4of4);
// 		$output = curl_exec($ch) or die('error ' . curl_error($ch));
// 		curl_close($ch);
// file_put_contents('/vagrant/uni-mall/page5.html', $output);
// \Log::info('5page,CLEAR!!');
// echo $output;
echo "------5-------------------------<br>";

/**
 * 6ページ目
 * @page メインメニュー
 * @note cookie取得 Read|Post
 */
 preg_match('/<a\s*href=\"(.*?)\">.*?デザイン設定.*?<\/a>/', $output, $sp);
		// $URL6of6 = 'https://mainmenu.rms.rakuten.co.jp/?left_navi=12';
		$URL6of6 = $sp[1];
		$ch = curl_init();
		// 詳細な情報を出力する
curl_setopt($ch, CURLOPT_VERBOSE, true);
// STDERR の代わりにエラーを出力するファイルポインタ
curl_setopt($ch, CURLOPT_STDERR, $log);
		curl_setopt($ch, CURLOPT_URL, $URL6of6);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
		curl_setopt($ch, CURLOPT_COOKIE, 'test=test');
		// curl_setopt($ch, CURLOPT_POST, TRUE);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $params4of4);
		// curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
		$output = curl_exec($ch) or die('error ' . curl_error($ch));
		curl_close($ch);
file_put_contents('/vagrant/uni-mall/page6.html', $output);
\Log::info('6page,CLEAR!!');
echo $output;
echo "-----------6--------------------<br>";

/**
 * 7ページ目
 * @page SP編集
 * @note cookie取得 Read|Post
 */
 // リンク取得
preg_match('/<a\s*href=\"(.*shop_bid.*)\">トップページ編集<\/a>/', $output, $sp);
\Log::info($sp);
		$URL7of7 = $sp[1];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
		// 詳細な情報を出力する
curl_setopt($ch, CURLOPT_VERBOSE, true);
// STDERR の代わりにエラーを出力するファイルポインタ
curl_setopt($ch, CURLOPT_STDERR, $log);
		curl_setopt($ch, CURLOPT_URL, $URL7of7);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
		// curl_setopt($ch, CURLOPT_POST, TRUE);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $params4of4);
		// curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
		$output = curl_exec($ch) or die('error ' . curl_error($ch));
		curl_close($ch);
file_put_contents('/vagrant/uni-mall/page7.html', $output);
// $fp = file_get_contents($fp);
\Log::info('7page,CLEAR!!');
echo $output;
echo "-----------------7--------------<br>";
// exit;
/**
 * 8ページ目
 * @page SP編集
 * @note cookie取得 Read|Post
 */
 // リンク取得
 preg_match('/<a\s*href=\"(.*?shop_bid.*?)\">トップページ編集<\/a>/', $output, $sp);
 var_dump($sp);
exit;
		$URL8of8 = $sp[1];
		$ch = curl_init();
curl_setopt($ch, CURLOPT_VERBOSE, true);
// STDERR の代わりにエラーを出力するファイルポインタ
curl_setopt($ch, CURLOPT_STDERR, $log);
		curl_setopt($ch, CURLOPT_URL, $URL8of8);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $fp);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $fp);
		// curl_setopt($ch, CURLOPT_POST, TRUE);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $params4of4);
		$output = curl_exec($ch) or die('error ' . curl_error($ch));
		curl_close($ch);
file_put_contents('/vagrant/uni-mall/page8.html', $output);
// $fp = file_get_contents($fp);

\Log::info('tobeContinued...');
/**
 * アウトプット
 * @page anywhere
 * @note cookie取得 Reading
 */
	    mb_language("Japanese");
	    $html_source = mb_convert_encoding($output, "UTF-8", "auto");

		// TODO:: 楽天のエラーメッセージを拾う
	    echo $html_source;



    //Cookieの削除のタイミングはあなたまかせです
    // unlink($cookie_path);

	}

}
