<?php namespace Valis\Trans;

use Illuminate\Support\ServiceProvider;
class TransServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    // protected $defer = false;
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('valis/trans');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Trans', function($app){
            return new Trans;
        });

        $this->app->bind('Socket', function($app){
            return new Net_Socket;
        });

        $this->app->bind('URL', function($app, $params){
            // return new Net_URL($params['url'], $params['brackets']);
            return new Net_URL($params['url'], $params['brackets']);
        });

        $this->app->bind('PEAR_Error', function($app){
            return new PEAR_Error;
        });

        $this->app->bind('Request', function($app, $params){
            $Req = new HTTP_Request;
            $Req->HTTP_Request($params['url'], $params['params']);
            return $Req;
        });

        $this->app->bind('Response', function($app, $params){
            $Res = new HTTP_Response;
            $Res->HTTP_Response($params['sock'], $params['listeners']);
            return $Res;
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('Trans');
    }

}
