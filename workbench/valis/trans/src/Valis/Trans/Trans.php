<?php namespace Valis\Trans;

// require_once("HTTP/Request.php");

class Trans {
	//my
	// var $api_key = '5009269e0269b63670860470434bfaf3ecb6bd1f';
	// var $api_secret_key = 'ee7dc4694e2233a21496de0bc8b39f1d865194a7';
	// default by account_misaki_ebay
	var $api_key = '8ef940eaca7420deada9864f6dfb2ab45d8f092c';
	var $api_secret_key = '5d4feee30eda6737b075808154cda28102f84c4a';
	var $token = '5a43415dbdba3d07d0b49c2cafdc432e42345f565a9fd33077c376ad406f1425';
	var $wasabi_api_hash = '203916a788aa313171d9c22e85b82cf9';

	var $api_url = 'http://www.world-honyaku.com/';


	// function __construct($wasabi_api_hash, $api_key=false, $api_secret_key=false) {
	//
	// 	$this->api_key = $api_key;
	// 	$this->api_secret_key = $api_secret_key;
	// 	$this->wasabi_api_hash = $wasabi_api_hash;
	// 	// $this->wasabi_api_hash = '203916a788aa313171d9c22e85b82cf9';
	//
	// 	//API等が既にある状態なら、アクセストークンを取得
	// 	if ($api_key && $api_secret_key && $wasabi_api_hash) {
	// 		$this->access_token();
	// 	}
	// }
	public function __construct() {

		$this->api_key = $this->api_key;
		$this->api_secret_key = $this->api_secret_key;
		$this->wasabi_api_hash = $this->wasabi_api_hash;

		$token = $this->access_token();
		$this->token = $token->data;
	}


	//代表者登録
	public function regist ($name, $password, $mail, $group_name) {

		// $req = new HTTP_Request($this->api_url.'api/regist');
		$req = \App::make('Request', ["url" => $this->api_url.'api/regist', 'params' => array()]);
		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('name', $name);
		$req->addPostData('password', $password);
		$req->addPostData('mail', $mail);
		$req->addPostData('group_name', $group_name);

		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();

			$res = json_decode($json_result);

			$this->api_key = $res->data->api_key;
			$this->api_secret_key = $res->data->api_secret;

			return $res;
		}

	}


	// 既存ユーザのAPIキー&APIシークレットキーを取得
	public function get_keys($mail) {

		// $req = new HTTP_Request($this->api_url.'api/reget_keys');
		$req = \App::make('Request', ["url" => $this->api_url.'api/reget_keys', 'params' => array()]);
		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('mail', $mail);


		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();
//var_dump($json_result);

			$res = json_decode($json_result);

			$this->api_key = $res->data->api_key;
			$this->api_secret_key = $res->data->api_secret;

			return $res;
		}

	}


	//アクセストークンを取得
	public function access_token() {

		// $req = new HTTP_Request($this->api_url.'api/access_token');
		$req = \App::make('Request', ["url" => $this->api_url.'api/access_token', 'params' => array()]);
		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('key', $this->api_key);
		$req->addPostData('secret', $this->api_secret_key);

		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();
//var_dump($json_result);
			$res = json_decode($json_result);
			$this->token = $res->data;

			return $res;
		}

	}


	//登録辞書をページごとに取得
	public function dictionary($before_lang, $after_lang, $page=1, $keyword='') {

		// $req = new HTTP_Request($this->api_url.'api/dictionary');
		$req = \App::make('Request', ["url" => $this->api_url.'api/dictionary', 'params' => array()]);
		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('token', $this->token);
		$req->addPostData('before_lang', $before_lang);
		$req->addPostData('after_lang', $after_lang);
		$req->addPostData('page', $page);
		$req->addPostData('keyword', $keyword);

		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();
//var_dump($json_result);
			$res = json_decode($json_result);

			return $res;
		}
	}


	//登録辞書をすべて取得
	public function dictionary_all($before_lang, $after_lang, $keyword='') {

		$dictionary = array();

		//まず1ページ目を取得
		$res = $this->dictionary($before_lang, $after_lang, 1, $keyword);
		foreach ($res->data->dictionary as $item) {
			$dictionary[] = (array)$item;
		}

		//総ページ数を取得
		$page_max = $res->data->page_num + 1;

		//2ページ以降があるなら繰り返して取得
		if ($page_max > 1) {
			for ($i=2; $i<=$page_max; $i++) {
				$res = $this->dictionary($before_lang, $after_lang, $i, $keyword);
				foreach ($res->data->dictionary as $item) {
					$dictionary[] = (array)$item;
				}
			}
		}

		return $dictionary;
	}

	//追加
	public function add($before_word, $after_word, $before_lang, $after_lang, $word_type) {

		// $req = new HTTP_Request($this->api_url.'api/dictionary/add');
		$req = \App::make('Request', ["$this->api_url.'api/dictionary/add'"]);
		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('token', $this->token);
		$req->addPostData('before_word', $before_word);
		$req->addPostData('after_word', $after_word);
		$req->addPostData('before_lang', $before_lang);
		$req->addPostData('after_lang', $after_lang);
		$req->addPostData('word_type', $word_type);

		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();
//var_dump($json_result);
			$res = json_decode($json_result);

			return $res;
		}
	}


	//編集
	public function edit($word_id, $after_word, $before_lang, $after_lang) {

		// $req = new HTTP_Request($this->api_url.'api/dictionary/edit');
		$req = \App::make('Request', ["$this->api_url.'api/dictionary/edit'"]);
		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('token', $this->token);
		$req->addPostData('word_id', $word_id);
		$req->addPostData('after_word', $after_word);
		$req->addPostData('before_lang', $before_lang);
		$req->addPostData('after_lang', $after_lang);

		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();
//var_dump($json_result);
			$res = json_decode($json_result);

			return $res;
		}
	}


	//削除
	public function delete($word_id, $before_lang, $after_lang) {

		// $req = new HTTP_Request($this->api_url.'api/dictionary/delete');
		$req = \App::make('Request', ["$this->api_url.'api/dictionary/delete'"]);
		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('token', $this->token);
		$req->addPostData('word_id', $word_id);
		$req->addPostData('before_lang', $before_lang);
		$req->addPostData('after_lang', $after_lang);

		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();
//var_dump($json_result);
			$res = json_decode($json_result);

			return $res;
		}
	}


	//翻訳
	public function trans($query, $before_lang, $after_lang) {

		// $req = new HTTP_Request($this->api_url.'api/trans');
		// $req = \App::make('Request', ['url' => $this->api_url.'api/trans', 'params' => array('user' => 'misaki@wasab.net', 'pass' => 'wasabisaikou')]);
		$req = \App::make('Request', ['url' => $this->api_url.'api/trans', 'params' => array()]);

		$req->addHeader("Wasabi-Api-Hash", $this->wasabi_api_hash);

		$req->setMethod(HTTP_REQUEST_METHOD_POST);
		$req->addPostData('token', $this->token);
		$req->addPostData('query', $query);
		$req->addPostData('before_lang', $before_lang);
		$req->addPostData('after_lang', $after_lang);
// dump_r($req);exit;
		$response = $req->sendRequest();

		if (PEAR::isError($response)) {
			$json_result = $response->getMessage();
			return false;
		} else {
			$json_result = $req->getResponseBody();
			$res = json_decode($json_result);
			return $res->data;
		}
	}

	/**
	* translate A word into B.
	* @param word 翻訳対象
	* @param ( arr ) country ('from' => A, 'to' => B) 翻訳国指定 A into B.
	* @return 単語
	*/

	public function translate($word, $country){

		//regist
		// $res = $this->regist('杉原直樹', 'Plusminus', 'sugihara.valis@icloud.com', 'Valis Inc.');
		// var_dump($res);exit;
		// getkey
		// $key = $this->get_keys('misaki@wasab.net');
		// var_dump($key);exit;

		// $key = $this->access_token('misaki@wasab.net');
		// var_dump($key);exit;

		// $key = $this->dictionary('ja', 'en');
		// var_dump($key);exit;


	    extract($country);

	    $translated = trim(strip_tags($this->trans($word, $from, $to)));
// dump_r($translated);exit;
	    return $translated;

	}


}
