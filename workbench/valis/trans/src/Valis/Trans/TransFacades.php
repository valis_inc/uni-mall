<?php
/**
* @author Valis
*/
namespace Valis\Trans;
use Illuminate\Support\Facades\Facade;

class TransFacades extends Facade{
    protected static function getFacadeAccessor(){
        return 'Trans';
    }
}
?>
