<?php

class RktAccount extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rkt_accounts';
	protected $guarded = array('user_id', 'ftp_account', 'ftp_password');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('ftp_account', 'ftp_password');



	public function user()
	{
		return $this->belongsTo('User');
	}

	public function contents()
	{
		return $this->hasMany('RktContent');
	}

	public function banners()
	{
		return $this->hasMany('Banners', 'account_id');
	}

	// --- Scopes ---
	public function scopeStore($query, $id)
	{
		return $query->whereUserId($id);
	}



}
