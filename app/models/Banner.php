<?php

class Banner extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'banners';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function rakuten()
	{
		return $this->belongsTo('RktAccount');
	}



}
