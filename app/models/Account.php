<?php

class Account extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'accounts';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('rakuten_pass', 'ebay_pass', 'yahoo_pass');



	public function user()
	{
		return $this->belongsTo('User');
	}



}
