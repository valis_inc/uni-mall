<?php

class RktContent extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rkt_contents';
	protected $guarded = array('accounts_id');

	public function account()
	{
		return $this->belongsTo('RktAccount');
	}

	public function scopeArrival($query)
	{
		return $query->orderBy('created_at', 'desc');
	}

	public function scopePopular($query)
	{
		return $query->orderBy('review_average', 'desc');
	}

	// public function scopeSale($query)
	// {
	// 	return $query->orderBy('created_at', 'desc');
	// }

	public function scopeReview($query)
	{
		return $query->orderBy('review_counts', 'desc');
	}

	// public function scopeFreeShip($query)
	// {
	// 	return $query->orderBy('created_at', 'desc');
	// }

}
