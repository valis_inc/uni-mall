<?php

class Rakuten extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rakutens';
	protected $guarded = array('ftp_account', 'ftp_password');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('ftp_account', 'ftp_password');



	public function user()
	{
		return $this->belongsTo('User');
	}



}
