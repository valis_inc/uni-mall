<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
Route::get('json', function()
{
	$path = public_path().'/templates/rakuten.json';
	$fg = file_get_contents($path);
	$j = json_decode($fg, true);
	dump_r($j);exit;
});
*/

/**
* Statics Pages
*/
Route::get('usage', 'StaticController@getUsage');
Route::get('price', 'StaticController@getPrice');
Route::get('faq', 'StaticController@getFaq');
Route::get('contact', 'StaticController@getContact');
Route::get('contact/send', 'FunctionController@sendMail');
Route::get('/', 'StaticController@getIndex');

/**
* Other Each
*/
Route::get('images/{store_id}/{type}', 'ImageController@display');


/**
* Controllers
*/
Route::controllers([
	'login' => 'LoginController',
]);
Route::controllers([
	'malls/rakuten' => 'RakutenController',
	'malls/ebay' => 'EbayController',
]);
Route::controller('password', 'RemindersController');
/**
* AuthGroups
*/
Route::group(['before' => 'auth'], function()
{
	Route::get('malls', 'MallController@getIndex');

});

/**
* Resources
*/
Route::resource('user', 'UserController', [
	'names' => ['create' => 'user.signup'],
	'uri' => ['user/create' => 'user/signup'],
]);

//-----forTest--------------
Route::get('test/{id}', 'RakutenController@test');
Route::get('curl', 'RakutenController@curl');
