@extends('layouts.master')


@section('content')
<div class="col-sm-12" id="login">
    {{Form::model('User', ['action' => 'LoginController@postIndex', 'class' => 'form-horizontal'])}}
    <!-- <form class="form-horizontal"> -->
    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>エラー！</strong>{{Session::get('error')}}
    </div>
    @endif
      <div class="form-group form-group-lg">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            {{Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email'])}}
          <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Email"> -->
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            {{Form::password('password', ['class' => 'form-control', 'placeholder' => 'password'])}}
          <!-- <input type="password" class="form-control" id="inputPassword3" placeholder="Password"> -->
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <div class="checkbox">
            <label>
                {{Form::checkbox('remember_token', '1')}}自動的にログイン
              <!-- <input type="checkbox"> Remember me -->
            </label>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {{Form::submit('サインイン', ['class' => 'btn btn-default'])}}
          <!-- <button type="submit" class="btn btn-default">Sign in</button> -->
        </div>
      </div>
    {{Form::close()}}

    <p class="bg-warning">{{link_to_route('user.signup', '新規登録の方はこちら')}}</p>
    <p class="bg-warning">{{link_to_action('RemindersController@getRemind', 'ID/パスワードを忘れた方はこちら')}}</p>

</div>
@stop
