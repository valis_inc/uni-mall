@extends('layouts.master')


@section('content')
<form action="{{ action('RemindersController@postReset') }}" method="POST">
    <input type="hidden" name="token" value="{{ $token }}">
    Email:<input type="email" name="email">
    Pass:<input type="password" name="password">
    Confirm:<input type="password" name="password_confirmation">
    <input type="submit" value="Reset Password">
</form>
@stop
