@extends('layouts.master')


@section('content')
<div id="rakuten">
    <h3>店舗作成</h3>

    <div class="list_accounts">
        <dl class="">
            <dt>ショップコード</dt>
            <dd>{{$store->shop_code}}</dd>
            <dt>ショップ名</dt>
            <dd>{{$store->shop_name}}</dd>
            <dt>ショップURL</dt>
            <dd>{{$store->shop_url}}</dd>
            <dt>デザインテンプレート</dt>
            <dd>{{$store->css_type}}</dd>
            <dt>キャプションワード</dt>
            <dd>{{$store->key_sales}}</dd>
            <dt>キーワードA</dt>
            <dd>{{$store->key_freeA}}</dd>
            <dt>キーワードB</dt>
            <dd>{{$store->key_freeB}}</dd>
            <dt>FTPアカウント</dt>
            <dd>{{$store->ftp_account}}</dd>
            <dt>FTPパスワード</dt>
            <dd>{{$store->ftp_password}}</dd>
        </dl>
        <br>
        <p>
            以上の設定でよろしいですか？
        </p>
        {{Form::open(['action' => 'RakutenController@postNewStore'])}}
        {{Form::hidden('store_id', $store->id)}}
        {{Form::submit('店舗を作成する')}}
        {{Form::close()}}
    </div>

</div>
@stop
