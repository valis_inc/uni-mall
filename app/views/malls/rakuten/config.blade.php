@extends('layouts.master')


@section('content')
<div id="rakuten">
    <h3>FTP設定</h3>

    {{HTML::Sessions()}}

    {{Form::model($RktAccount, ['action' => 'RakutenController@postConfig'])}}
    {{Form::hidden('id', $id)}}
      <div class="form-group">
        <label for="ftp_account">店舗アカウント</label>
        {{Form::text('ftp_account', null, ['class' => 'form-control', 'placeholder' => 'FTPアカウントを入力してください'])}}
      </div>
      <div class="form-group">
        <label for="ftp_password">パスワード</label>
        {{Form::password('ftp_password', ['class' => 'form-control', 'placeholder' => 'FTPパスワードを入力してください'])}}
      </div>
      {{Form::submit('設定する', ['class' => 'btn btn-default'])}}
    {{Form::close()}}
</div>
@stop
