@extends('layouts.master')


@section('content')
<div id="rakuten">
    <h3>商品の詳細な設定を行います</h3>

    {{HTML::Sessions()}}

    <div class="details">

    {{Form::model($RktAccount, ['action' => 'RakutenController@postDetail'])}}
    {{Form::hidden('id', $id)}}
      <div class="form-group">
        <label for="key_sales">SALEキーワード</label>
        {{Form::text('key_sales', null, ['class' => 'form-control', 'placeholder' => 'SALEアイテムのキーワードを入力してください'])}}
      </div>
      <div class="form-group">
        <label for="key_freeA">フリーワードA</label>
        {{Form::text('key_freeA', null, ['class' => 'form-control', 'placeholder' => 'フリーワードA'])}}
      </div>
      <div class="form-group">
        <label for="key_freeB">フリーワードB</label>
        {{Form::text('key_freeB', null, ['class' => 'form-control', 'placeholder' => 'フリーワードB'])}}
      </div>
      {{Form::submit('設定する', ['class' => 'btn btn-default'])}}
    {{Form::close()}}

    </div>
</div>
@stop
