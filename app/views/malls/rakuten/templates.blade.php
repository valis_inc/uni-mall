@extends('layouts.master')


@section('content')
<div id="rakuten">
    <h3>デザインテンプレート選択</h3>

    {{HTML::Sessions()}}

    {{Form::model($RktAccount, ['action' => 'RakutenController@postTemplates'])}}
    {{Form::hidden('id', $id)}}
      <div class="form-group">
        <h4>レイアウト</h4>
        {{Form::radio('css_type.0', 'colum2', ['class' => 'form-control'])}}
        <label for="exampleInputEmail1">２カラム</label>
      </div>
      <div class="form-group">
        <h4>カラー</h4>
        {{Form::radio('css_type.1', 'black', ['class' => 'form-control'])}}
        <label for="exampleInputEmail1">黒</label>
        {{Form::radio('css_type.1', 'red', ['class' => 'form-control'])}}
        <label for="exampleInputEmail1">赤</label>
      </div>
      {{Form::submit('登録する', ['class' => 'btn btn-default'])}}
    {{Form::close()}}
</div>
@stop
