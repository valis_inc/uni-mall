@extends('layouts.master')


@section('content')
<div id="rakuten">
    <h3>楽天市場店舗の追加（左サイドナビに追加されます）</h3>

    @if (Session::has('success'))
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong>{{Session::get('success')}}
    </div>
    @endif

    @if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Error!</strong>{{Session::get('error')}}
    </div>
    @endif

    {{Form::open()}}
      <div class="form-group">
        {{Form::label('shopCode', 'アカウント名')}}
        {{Form::text('shopCode', '', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'http://www.rakuten.ne.jp/gold/*******/'], 'Accounts')}}
        <caption>※↑*****の箇所を入力ください</caption>
      </div>
      <div class="form-group">
          {{Form::select('genre', $genre, '', ['class' => 'form-control'])}}
      </div>
      {{Form::submit('登録する', ['class' => 'btn btn-default'])}}
    {{Form::close()}}
</div>
@stop
