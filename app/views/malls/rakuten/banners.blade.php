@extends('layouts.master')


@section('content')
<div id="rakuten">
    <h3>バナー画像設定</h3>

    {{HTML::Sessions()}}

    {{Form::model('RktAccount', ['action' => 'RakutenController@postBanners', 'files' => true])}}
        {{Form::hidden('shop_id', $shop_id)}}

      <div class="form-group">
        {{Form::label('banner_top', 'TOPバナー')}}
        {{Form::file('banner_top')}}
        <p class="help-block">バナーサイズは200x320となります</p>
        <section class='thumb_area'>
            <?php $path = action('ImageController@display', [$shop_id, 'top']); ?>
            <img src="{{$path}}" alt="" />
        </section>
      </div>

      <div class="form-group">
        {{Form::label('banner_bottom', 'Bottomバナー')}}
        {{Form::file('banner_bottom')}}
        <p class="help-block">バナーサイズは200x320となります</p>
        <section class='thumb_area'>
            <?php $path = action('ImageController@display', [$shop_id, 'bottom']); ?>
            <img src="{{$path}}" alt="" />
        </section>
      </div>

      {{Form::submit('登録する', ['class' => 'btn btn-default'])}}
    {{Form::close()}}
</div>
@stop
