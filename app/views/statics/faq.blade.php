@extends('layouts.master')


@section('content')

    <?php $country = ['from' => 'ja', 'to' => 'zh-CN']; ?>
    <p>{{Trans::translate('よくある質問', $country)}}よくある質問</p>
    <p>{{Trans::translate('料金', $country)}}料金</p>
    <p>{{Trans::translate('カテゴリー', $country)}}カテゴリー</p>
    <p>{{Trans::translate('メルマガ', $country)}}メルマガ</p>
    <p>{{Trans::translate('レビュー', $country)}}レビュー</p>
    <p>{{Trans::translate('ガイド', $country)}}ガイド</p>
    <p>{{Trans::translate('創業60年の老舗質屋だからできる！安心の高価買取！', $country)}}創業60年の老舗質屋だからできる！安心の高価買取！</p>
    <p>{{Trans::translate('アイテムカテゴリ', $country)}}アイテムカテゴリ</p>
    <p>{{Trans::translate('バッグ', $country)}}バッグ</p>
    <p>{{Trans::translate('腕時計', $country)}}腕時計</p>
    <p>{{Trans::translate('ジュエリー', $country)}}ジュエリー</p>
    <p>{{Trans::translate('ファッション', $country)}}ファッション</p>
    <p>{{Trans::translate('その他', $country)}}その他</p>
    <p>{{Trans::translate('カテゴリ一覧', $country)}}カテゴリ一覧</p>
    <p>{{Trans::translate('会社概要', $country)}}会社概要</p>
    <p>{{Trans::translate('お買い物ガイド（PC）', $country)}}お買い物ガイド（PC）</p>
    <p>{{Trans::translate('カートを見る', $country)}}カートを見る</p>
    <p>{{Trans::translate('ショップレビュー', $country)}}ショップレビュー</p>
    <p>{{Trans::translate('メールマガジン登録', $country)}}メールマガジン登録</p>
    <p>{{Trans::translate('最新ニュースやお得なキャンペーン情報を', $country)}}最新ニュースやお得なキャンペーン情報を</p>
    <p>{{Trans::translate('メルマガ会員様だけにいち早くお届け！', $country)}}メルマガ会員様だけにいち早くお届け！</p>
    <p>{{Trans::translate('ブランド京の蔵小牧トップ', $country)}}ブランド京の蔵小牧トップ</p>
    <p>{{Trans::translate('楽天トップ', $country)}}楽天トップ</p>
    <p>{{Trans::translate('表示', $country)}}表示</p>
    <p>{{Trans::translate('スマートフォン版', $country)}}スマートフォン版</p>
    <p>{{Trans::translate('PC版', $country)}}PC版</p>
    <p>{{Trans::translate('ブランド京の蔵小牧 ALL Right Reserved.', $country)}}ブランド京の蔵小牧</p>
    <p>{{Trans::translate('京の蔵 小牧【最安挑戦】', $country)}}京の蔵</p>
    <p>{{Trans::translate('アイテムを検索する', $country)}}アイテムを検索する</p>
    <p>{{Trans::translate('配信先メールアドレス', $country)}}配信先メールアドレス</p>
    <p>{{Trans::translate('スマートフォンテンプレート', $country)}}スマートフォンテンプレート</p>
    <p>{{Trans::translate('トップバナー', $country)}}トップバナー</p>
@stop
