@extends('layouts.master')

@section('content')
<div class="col-sm-12" id="login">
    {{Form::open()}}
    <table class="table">
        <thead>
            <tr>
                <th>
                    サインアップ
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>
                    {{Form::label('name', 'ログイン名')}}
                </th>
                <td>
                    {{Form::text('name')}}
                </td>
            </tr>
            <tr>
                <th>
                    {{Form::label('email', 'メールアドレス')}}
                </th>
                <td>
                    {{Form::text('email')}}
                </td>
            </tr>
            <tr>
                <th>
                    {{Form::label('pass', 'パスワード')}}
                </th>
                <td>
                    {{Form::text('pass')}}
                </td>
            </tr>
            <tr>
                <th>
                    {{Form::label('pass', 'パスワード確認')}}
                </th>
                <td>
                    {{Form::text('pass_confirm')}}
                </td>
            </tr>
{{--
            <tr>
                <td>
                    {{Form::check('remember_me')}}
                </td>
            </tr>
--}}
            <tr>
                <td>
                    {{Form::submit('登録', array('class' => 'btn btn-primary'))}}
                </td>
            </tr>
        </tbody>
    </table>
    {{Form::close()}}
</div>
@stop
