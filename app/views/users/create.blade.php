@extends('layouts.master')

@section('content')
<div class="col-sm-12" id="login">

    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>エラー！</strong>{{Session::get('err')}}
    </div>

    <div class="col-sm-12" id="signup">
        {{Form::model('User', ['route' => 'user.store', 'class' => 'form-horizontal'])}}
        <div class="form-group form-group-lg">
            {{Form::label('name', 'UserName', ['class' => 'col-sm-2 control-label'])}}
          <div class="col-sm-10">
            {{Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'UserName'])}}
          </div>
        </div>
          <div class="form-group form-group-lg">
              {{Form::label('email', 'Email', ['class' => 'col-sm-2 control-label'])}}
            <div class="col-sm-10">
                {{Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email'])}}
            </div>
          </div>
          <div class="form-group form-group-lg">
              {{Form::label('password', 'Password', ['class' => 'col-sm-2 control-label'])}}
            <div class="col-sm-10">
                {{Form::password('password', ['class' => 'form-control', 'placeholder' => 'password'])}}
            </div>
          </div>
          <div class="form-group form-group-lg">
              {{Form::label('password_confirmation', 'Confirm', ['class' => 'col-sm-2 control-label'])}}
            <div class="col-sm-10">
                {{Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'password'])}}
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{Form::submit('サインアップ', ['class' => 'btn btn-default'])}}
            </div>
          </div>
        {{Form::close()}}
    </div>
    @stop


{{--
    <table class="table">
        <thead>
            <tr>
                <th>
                    ユーザーCREATE
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>
                    {{Form::label('name', 'ログイン名')}}
                </th>
                <td>
                    {{Form::text('name')}}
                </td>
            </tr>
            <tr>
                <th>
                    {{Form::label('email', 'メールアドレス')}}
                </th>
                <td>
                    {{Form::text('email')}}
                </td>
            </tr>
            <tr>
                <th>
                    {{Form::label('password', 'パスワード')}}
                </th>
                <td>
                    {{Form::password('password')}}
                </td>
            </tr>
            <tr>
                <th>
                    {{Form::label('password_confirmation', 'パスワード確認')}}
                </th>
                <td>
                    {{Form::password('password_confirmation')}}
                </td>
            </tr>
            <tr>
                <td>
                    {{Form::check('remember_me')}}
                </td>
            </tr>
            <tr>
                <td>
                    {{Form::submit('登録', array('class' => 'btn btn-primary'))}}
                </td>
            </tr>
        </tbody>
    </table>
    {{Form::close()}}
--}}

</div>

@stop
