<div id="new_arrival" class="col-xs-offset-1 col-xs-10">
    <h2>新着アイテム</h2>
    <?php foreach ($contents['new'] as $content): ?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <a href="<?php echo $content->item_url ?>">
                    <img src="<?php echo $content->small_image_url_A ?>" alt="アイテム画像" />
                </a>
            </div>
            <div class="descripstion">
                <?php echo $content->item_name ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div id="pick_up" class="col-xs-offset-1 col-xs-10">
    <h2>売れ筋ピックアップ</h2>
    <?php foreach ($contents['popular'] as $content): ?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <a href="<?php echo $content->item_url ?>">
                    <img src="<?php echo $content->small_image_url_A ?>" alt="アイテム画像" />
                </a>
            </div>
            <div class="descripstion">
                <?php echo $content->item_name ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div id="sale_item" class="col-xs-offset-1 col-xs-10">
    <h2>セール商品</h2>
    <div class="showcase col-xs-5">
        <div class="thumb">
            <a href="<?php echo $content->item_url ?>">
                <img src="<?php echo $content->small_image_url_A ?>" alt="アイテム画像" />
            </a>
        </div>
        <div class="descripstion">
            <?php echo $content->item_name ?>
        </div>
    </div>
</div>
<div id="popular" class="col-xs-offset-1 col-xs-10">
    <h2>レビュー多数！人気商品</h2>
    <?php foreach ($contents['review'] as $content): ?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <a href="<?php echo $content->item_url ?>">
                    <img src="<?php echo $content->small_image_url_A ?>" alt="アイテム画像" />
                </a>
            </div>
            <div class="descripstion">
                <?php echo $content->item_name ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div id="free_shipping" class="col-xs-offset-1 col-xs-10">
    <h2>送料無料</h2>
    <div class="showcase col-xs-5">
        <div class="thumb">
            <a href="<?php echo $content->item_url ?>">
                <img src="<?php echo $content->small_image_url_A ?>" alt="アイテム画像" />
            </a>
        </div>
        <div class="descripstion">
            <?php echo $content->item_name ?>
        </div>
    </div>
</div>
