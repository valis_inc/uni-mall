<?php
  //set headers to NOT cache a page
  // header("Access-Control-Allow-Headers: Origin, x-requested-with, content-type, accept");
  // header("Access-Control-Allow-Origin: *");
  header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
  // header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>
<!-- <head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
</head> -->
    <h3><?php echo $contents['statics']['title01'];?></h3>
    <?php if (empty($contents['new'])):?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <?php echo $contents['statics']['sentence01'];?>
            </div>
            <div class="descripstion">
            </div>
        </div>
    <?php else: ?>
        <div class="itemskin1">
            <div class="slider multiple-items">
                <?php foreach ($contents['new'] as $content): ?>
                <div>
                    <a href="<?php echo $content['itemUrl'] ?>" target="_top">
                    <img src="<?php echo $content['mediumImageUrls'][0]['imageUrl'] ?>" alt="" class="photo"/>
                    <p class="name1"><?php echo $content['itemName'] ?></p>
                    <span class="plice"><?php echo $content['itemPrice'] ?><?php echo $contents['statics']['currency'];?></span>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="itembutton"><a href=""><?php echo $contents['statics']['sentence02'];?><span class="arrow_carrot-right_alt2"></span></a></div>

    <h3><?php echo $contents['statics']['title02'];?></h3>
    <?php if (empty($contents['sales'])):?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <?php echo $contents['statics']['sentence01'];?>
            </div>
            <div class="descripstion">
            </div>
        </div>
    <?php else: ?>
    <div class="itemskin1">
          <div class="slider multiple-items">
            <?php foreach ($contents['sales'] as $content): ?>
            <div>
                <a href="<?php echo $content['itemUrl'] ?>" target="_top">
                <img src="<?php echo $content['mediumImageUrls'][0]['imageUrl'] ?>" alt="" class="photo" />
                <p class="name1"><?php echo $content['itemName'] ?></p>
                <span class="plice"><?php echo $content['itemPrice'] ?><?php echo $contents['statics']['currency'];?></span>
                </a>
            </div>
            <?php endforeach; ?>
          </div>
    </div>
    <?php endif; ?>
    <div class="itembutton"><a href=""><?php echo $contents['statics']['sentence02'];?><span class="arrow_carrot-right_alt2"></span></a></div>

    <h3><?php echo $contents['statics']['title03'];?></h3>
    <?php if (empty($contents['freeA'])):?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <?php echo $contents['statics']['sentence01'];?>
            </div>
            <div class="descripstion">
            </div>
        </div>
    <?php else: ?>
    <div class="itemskin2">
        <?php foreach ($contents['freeA'] as $content): ?>
    	<div class="itemWrapper">
            <a href="<?php echo $content['itemUrl'] ?>">
                <img src="<?php echo $content['mediumImageUrls'][0]['imageUrl'] ?>" alt="" />
                <div class="textBox">
                    <p class="name">
                        <?php echo $content['itemName'] ?>
                        <span class="plice"><?php echo $content['itemPrice'] ?><?php echo $contents['statics']['currency'];?></span>
                    </p>
                </div>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>

    <h3><?php echo $contents['statics']['title04'];?></h3>
    <?php if (empty($contents['point'])):?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <?php echo $contents['statics']['sentence01'];?>
            </div>
            <div class="descripstion">
            </div>
        </div>
    <?php else: ?>
        <div class="itemskin1">
              <div class="slider multiple-items">
                <?php foreach ($contents['point'] as $content): ?>
                <div><a href="<?php echo $content['itemUrl'] ?>">
                    <img src="<?php echo $content['mediumImageUrls'][0]['imageUrl'] ?>" alt="" />
                    <p class="name1"><?php echo $content['itemName'] ?></p>
                    <span class="plice"><?php echo $content['itemPrice'] ?><?php echo $contents['statics']['currency'];?></span>
                    </a>
                </div>
                <?php endforeach; ?>
              </div>
        </div>
    <?php endif; ?>

    <h3><?php echo $contents['statics']['title05'];?></h3>
    <?php if (empty($contents['freeB'])):?>
        <div class="showcase col-xs-5">
            <div class="thumb">
                <?php echo $contents['statics']['sentence01'];?>
            </div>
            <div class="descripstion">
            </div>
        </div>
    <?php else: ?>
        <div class="itemskin1">
              <div class="slider multiple-items">
                <?php foreach ($contents['freeB'] as $content): ?>
                <div><a href="<?php echo $content['itemUrl'] ?>">
                    <img src="<?php echo $content['mediumImageUrls'][0]['imageUrl'] ?>" alt="" />
                    <p class="name1"><?php echo $content['itemName'] ?></p>
                    <span class="plice"><?php echo $content['itemPrice'] ?><?php echo $contents['statics']['currency'];?></span>
                    </a>
                </div>
                <?php endforeach; ?>
              </div>
        </div>
    <?php endif; ?>

<h3><?php echo $contents['statics']['title06'];?></h3>
<div class="Bannerskin1">
	<ul>
    	<li><a href=""><img src="http://image.rakuten.co.jp/kyounokura/cabinet/foerign/img64517745.jpg"></a></li>
    	<li><a href=""><img src="http://image.rakuten.co.jp/kyounokura/cabinet/foerign/img64517776.jpg"></a></li>
    	<li><a href=""><img src="http://image.rakuten.co.jp/kyounokura/cabinet/foerign/img64517796.jpg"></a></li>
    	<li><a href=""><img src="http://image.rakuten.co.jp/kyounokura/cabinet/foerign/img64517745.jpg"></a></li>
    </ul>
</div>

<div class="itembutton"><a href=""><?php echo $contents['statics']['sentence03'];?><span class="arrow_carrot-right_alt2"></span></a></div>
