<!doctype html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);
	</style>
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssreset/cssreset-min.css">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	{{HTML::style('styles.css')}}
	{{HTML::script('js/function.js')}}
</head>
<body>
	<div id="header" class="container-fluid">
		<div class="row">
		@section('header')
			<ul class="list-inline">
				<li><a href="/"><h1>WASAOU</h1></a></li>
				<li>{{link_to_action('StaticController@getUsage', 'ご利用方法')}}</li>
				<li>{{link_to_action('StaticController@getPrice', '料金')}}</li>
				<li>{{link_to_action('StaticController@getFaq', 'よくある質問')}}</li>
				<li>{{-- link_to_action('LoginController@anyLogout', 'ログアウト') --}}</li>
				<li>{{link_to_action('StaticController@getContact', 'お問い合わせ')}}</li>
				<li>testuser, test@gmail.com, test</li>
				@if(Auth::check())
				<li class="logout">{{link_to_action('LoginController@anyLogout', 'ログアウト')}}</li>
				@else
				<li class="login">{{link_to_action('LoginController@getIndex', 'ログイン')}}</li>
				@endif
			</ul>
		@show
		</div>
	</div>
	<div id="content">
		<div class="container-fluid">
			<div class="row">
				@section('sidebar')
				<div id="sidebar" class="col-sm-3">
					<nav id="side_manu">
						<ul class="list-unstyled">
							<h3>編集メニュー</h3>
							<li>作成ツール</li>
							<ul>
								@if ( Auth::check() )
								<li>
									<ul>
										<li>+楽天</li>
										<li>{{link_to_action('RakutenController@getAdd', '店舗追加')}}</li>
										@foreach( Session::get('stores', [] ) as $store )
										<ul>
											<li>{{$store->shop_name}}</li>
											<li>{{link_to_action('RakutenController@getDetail', 'キーワード詳細設定', ['shop_id' => $store->id] )}}</li>
											<li>{{link_to_action('RakutenController@getTemplates', 'テンプレート選択', ['shop_id' => $store->id] )}}</li>
											<li>{{link_to_action('RakutenController@getBanners', 'バナー設定', ['shop_id' => $store->id] )}}</li>
											<li>{{link_to_action('RakutenController@getConfig', 'FTP設定', ['shop_id' => $store->id] )}}</li>

											{{Form::open(['action' => 'RakutenController@postMake'])}}
											{{Form::hidden('store_id', $store->id)}}
											{{Form::submit('作成する')}}
											{{Form::close()}}
										</ul>
										@endforeach
									</ul>
								</li>
								{{--
								<li>
									<ul>
										<li>+ebay</li>
										<li>{{link_to_action('RakutenController@getAdd', '店舗追加')}}</li>
										<li>{{link_to_action('RakutenController@getDetail', 'スマホ商品ページ詳細作成')}}</li>
										<li>{{link_to_action('RakutenController@getConfig', 'FTP設定')}}</li>
									</ul>
								</li>
								--}}
								@endif
								<li>
									<ul>
										<li>ヘルプ</li>
										<li>{{link_to_action('StaticController@getUsage', 'ご利用方法')}}</li>
										<li>{{link_to_action('StaticController@getFaq', 'よくある質問')}}</li>
										<li>{{link_to_action('StaticController@getContact', 'お問い合わせ')}}</li>
									</ul>
								</li>
							</ul>
						</ul>
					</nav>
				</div>
				@show
				<div id="content" class="col-sm-9">
				@yield('content')
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		@section('footer')
		@show
	</div>
</body>
</html>
