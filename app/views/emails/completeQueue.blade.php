<!DOCTYPE HTML>
<html lang="ja">
<head>
	<meta charset="utf-8" />
	<title>作成完了</title>
</head>
<body>
	店舗の作成が完了しました。<br>
	<a href="{{$url}}">こちら</a>から確認できます。
</body>
</html>
