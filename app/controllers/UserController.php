<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		return View::make('users.index')->with('users', $users);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}
	// public function signup()
	// {
	// 	return View::make('users.signup');
	// }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::all();

		//validation
		$validator = Validator::make(
		    array(
				'name' => $inputs['name'],
				'email' => $inputs['email'],
				'password' => $inputs['password'],
			),
		    array(
				'name' => ['required'],
				'email' => ['required', 'email', 'unique:users'],
				'password' => ['required']
				// 'password' => ['required', 'confirmed']
			)
		);
		if ($validator->fails())
		{
			$messages = $validator->messages();
			return Redirect::route('user.signup')->with('err', $messages);
		}

		//insert
		$user = User::create($inputs);
		//pass::hashed
		$password = Hash::make($inputs['password']);
		$user->password = $password;
		$user->save();
		Log::info($inputs);
		return Redirect::action('LoginController@getIndex');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return 'show';
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return 'edit'. $id;
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return 'update';
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return 'destroy';
	}


}
