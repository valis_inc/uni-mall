<?php

// use Valis\Trans\Trans;
// use Valis\Trans\TransFacades;
// use Trans;

/**
* Redis Rules
* ( value ) TABLENAME : PRIMARYKEY : COLUMNNAME
* ( object ) TABLENAME :: PRIMARYKEY
*/

class RakutenController extends BaseController {
	protected $layout = 'layouts.master';
	// Mallインスタンス
	protected $Unimall;
	// 登録店舗
 	protected $stores;

	public function __construct(\Valis\Unimall\Rakuten $mall)
	{
		$this->beforeFilter('auth', ['except' => 'getApi'] );
		// Mallを実体化
		$this->Unimall = $mall;
	}

	/**
	* getMethods
	* @notes	ViewPageFunctions
	*/
	public function getIndex()
	{
		// $Unimall = App::make('Unimall');
		$genre = $this->Unimall->genre;
		// return View::make('malls.rakuten.index', compact('genre'));
		return View::make('malls.rakuten.index');
	}

	public function getAdd()
	{
		$genre = $this->Unimall->genre;
		return View::make('malls.rakuten.add', compact('genre'));
	}

	public function getDetail($store_id = null)
	{
		$RktAccount = RktAccount::findOrFail($store_id);
		return View::make('malls.rakuten.detail', compact('RktAccount'))->with('id', $store_id);
	}

	public function getConfig($store_id = null)
	{
		$RktAccount = RktAccount::findOrFail($store_id);
		return View::make('malls.rakuten.config', compact('RktAccount'))->with('id', $store_id);
	}

	public function getTemplates($store_id = null)
	{
		$RktAccount = RktAccount::findOrFail($store_id);
		return View::make('malls.rakuten.templates', compact('RktAccount'))->with('id', $store_id);
	}

	public function getBanners($store_id = null)
	{
		// todo::バナー画像出し
		// todo::拡張子選択

		// $image = Image::make(file_get_contents('http://goo.gl/uDTEzv'));
		$top_banner = app_path(). '/images/' .$store_id. '/top_banner.png';
		$bottom_banner = app_path(). '/images/' .$store_id. '/bottom_banner.png';

		//$image->resize(100,100);

		// アスペクト比等倍
		// $image->resize(100, null, function ($constraint) {
		// 	$constraint->aspectRatio();
		// });
		// クロップ
		// $image->crop(100, 100);
		// 左上からクロップする
		// $image->crop(100, 100, 0, 0);
		// グレースケール
		// $image->greyscale();

		// $image->resize(100, null, function($constraint){
		// 	$constraint->aspectRatio();
		// })->crop(50,50,0,0)->greyscale();

		// return $image->response('jpg');
		return View::make('malls.rakuten.banners', compact('top_banner', 'bottom_banner'))->with('shop_id', $store_id);
	}


	/**
	* postMethods
	* @note setDataFunctions
	*/
	// todo:: Validation, gets from AuthData
	public function postAdd()
	{
		$inputs = Input::all();
		// 変数として抽出
		extract($inputs);

		// SDKで店舗情報を取得 todo::App::make(*)
		$Unimall = $this->Unimall;
		// Defaults availability => 1
		$items = $Unimall->searchShopCode($shopCode);
		if( $items !== false && isset($items[0]['shopCode']) )
		{
			// $account = new RktAccount;
			$account = RktAccount::firstOrCreate(['shop_code' => $shopCode]);
			$account->user_id 				= Auth::id();
			$account->shop_code 			= $shopCode;
			$account->shop_name 			= $items[0]['shopName'];
			$account->shop_url 				= $items[0]['shopUrl'];
			$account->shop_affiliate_url 	= $items[0]['shopAffiliateUrl'];
			//$accounts->genre_id = Auth::user('id');
			$account->save();

			$new_id = $account->id;

			// Session に保存し直し
			$stores = User::findOrFail(Auth::id())->rakuten;
			Session::put('stores', $stores);

// Ver.1.0 with MySQL
// 			foreach ($items as $item)
// 			{
// 				// $content = new RktContent;
// $content = RktContent::firstOrCreate(['accounts_id' => $new_id]);
// dump_r($content);exit;
// 				$content->accounts_id 		= $new_id;
// 				$content->item_name 		= $item['itemName'];
// 				$content->item_url 			= $item['itemUrl'];
// 				$content->image_flag 		= $item['imageFlag'];
// 				$content->small_image_url_A = ( isset($item['smallImageUrls'][0]['imageUrl']) ) ? $item['smallImageUrls'][0]['imageUrl'] : null;
// 				$content->small_image_url_B = ( isset($item['smallImageUrls'][1]['imageUrl']) ) ? $item['smallImageUrls'][1]['imageUrl'] : null;
// 				$content->small_image_url_C = ( isset($item['smallImageUrls'][2]['imageUrl']) ) ? $item['smallImageUrls'][2]['imageUrl'] : null;
// 				$content->medium_image_url_A = ( isset($item['meduimImageUrls'][0]['imageUrl']) ) ? $item['meduimImageUrls'][0]['imageUrl'] : null;
// 				$content->medium_image_url_B = ( isset($item['meduimImageUrls'][1]['imageUrl']) ) ? $item['meduimImageUrls'][1]['imageUrl'] : null;
// 				$content->medium_image_url_C = ( isset($item['meduimImageUrls'][2]['imageUrl']) ) ? $item['meduimImageUrls'][2]['imageUrl'] : null;
// 				$content->ship_overseas_flag= $item['shipOverseasFlag'];
// 				$content->ship_overseas_area= $item['shipOverseasArea'];
// 				$content->review_counts		= $item['reviewCount'];
// 				$content->review_average	= $item['reviewAverage'];
//
// 				$content->save();
// 			}

		// Mod for Change Models 150529
		// // todo:: gets from AuthData, modify $items[0]['genreId']
		// // $shop = Rakuten::firstOrCreate(['shop_code' => $shopCode]);
		// $shop = new Rakuten;
		// // $shop->user_id = $userId;
		// $shop->shop_code = $shopCode;
		// $shop->shop_name = $items[0]['shopName'];
		// $shop->shop_url = $items[0]['shopUrl'];
		// // $shop->shop_affiliate_url = $shopAffiliateUrl;
		// $shop->genre_id = $items[0]['genreId'];
		// // $shop->ftp_account = $ftpAccount;
		// // $shop->ftp_password = $ftpPassword;
		// $shop->save();

			return Redirect::action('RakutenController@getAdd')->with('success', '登録しました');
		}

		return Redirect::action('RakutenController@getAdd')->with('error', '楽天店舗が見つかりませんでした');
	}

	public function postDetail()
	{
		$inputs = Input::all();
		$RktAccount = RktAccount::findOrFail($inputs['id']);
		if ($RktAccount)
		{
			$RktAccount->key_sales = $inputs['key_sales'];
			$RktAccount->key_freeA = $inputs['key_freeA'];
			$RktAccount->key_freeB = $inputs['key_freeB'];
			$RktAccount->save();

			Session::flash('success', '登録しました');
			return Redirect::action('RakutenController@getDetail', ['shop_id' => $inputs['id']])->with('success', '登録しました');
		}
		return Redirect::action('RakutenController@getDetail', ['shop_id' => $inputs['id']])->with('error', '登録に失敗しました');
	}

	public function postConfig()
	{
		$inputs = Input::all();
		// user()->email から 店舗アカウントに変更（複数ユーザー同一店舗利用対応）
		// $unique_key = Hash::make(Auth::user()->email);
		$unique_key = Hash::make($inputs['ftp_account']);
		$ftp = [
			'user' => $inputs['ftp_account'],
			'password' => $inputs['ftp_password'],
			'unique_key' => $unique_key,
			];
		// FTP接続判定
		$result = $this->Unimall->ftpConnect($ftp, false);
		if($result['flg'])
		{
			// 成功時のみテーブルに保存
			// todo::パスワードハッシュ
			$RktAccount = RktAccount::findOrFail($inputs['id']);
			$RktAccount->ftp_account = $inputs['ftp_account'];
			$RktAccount->ftp_password = $inputs['ftp_password'];
			if($RktAccount->save())
			{
				return Redirect::action('RakutenController@getConfig', ['shop_id' => $inputs['id']])->with('success', '設定が完了しました');
			}
			else
			{
				return Redirect::action('RakutenController@getConfig', ['shop_id' => $inputs['id']])->with('error', 'データベースへ登録できませんでした');
			}
		}
		else
		{
			return Redirect::action('RakutenController@getConfig', ['shop_id' => $inputs['id']])->with('error', $result['msg']);
		}
	}

	public function postBanners()
	{
		$inputs = Input::all();
		$msg['flg'] = null;
		$msg['body'] = null;

		// todo::条件分け判別
		// 画像取得パート
		// $filename =  $inputs['banner_top']->getClientOriginalName();
		if ($inputs['shop_id'])
		{
			$dir = app_path(). '/images/' .$inputs['shop_id'];
			if (!file_exists($dir))
			{
				mkdir($dir, 0777);
			}
			if (isset($inputs['banner_top']))
			{
				$banner_top = Image::make($inputs['banner_top']->getRealPath());
				$ext_top = $inputs['banner_top']->getClientOriginalExtension();
				$banner_top->save($dir . '/top_banner' . '.' .$ext_top);

				$msg['flg'] = 'success';
				$msg['body'] .= "TOPバナーを設定しました&nbsp&nbsp";
			}
			if (isset($inputs['banner_bottom']))
			{
				$banner_bottom = Image::make($inputs['banner_bottom']->getRealPath());
				$ext_bottom = $inputs['banner_bottom']->getClientOriginalExtension();
				$banner_bottom->save($dir . '/bottom_banner' . '.' .$ext_bottom);

				$msg['flg'] = 'success';
				$msg['body'] .= "Bottomバナーを設定しました<br>";
			}
		}
		if ( $msg['flg']==null && $msg['body']==null )
		{
			$msg['flg'] = 'error';
			$msg['body'] = 'バナーを設定できませんでした';
		}

		return Redirect::action('RakutenController@getBanners', ['shop_id' => $inputs['shop_id']])->with($msg['flg'], $msg['body']);
	}

	public function postTemplates()
	{
		$id = Input::get('id', null);
		if ( is_null($id) )
		{
			return Redirect::action('RakutenController@getTemplates')->with('error', 'アカウントが見つかりません');
		}
		$layout = Input::get('css_type_0', null);
		$color = Input::get('css_type_1', null);

		$RktAccount = RktAccount::findOrFail($id);
		$RktAccount->css_type = "$layout$color";
		if($RktAccount->save())
		{
			return Redirect::action('RakutenController@getTemplates', ['id' => $id])->with('success', '設定が完了しました');
		}
		else
		{
			return Redirect::action('RakutenController@getTemplates', ['id' => $id])->with('error', 'データベースへ登録できませんでした');
		}
	}


	public function postMake()
	{
		$store_id = Input::get('store_id', null);
		if ( is_null($store_id) )
		{
			return Redirect::action('RakutenController@getIndex')->with('error', 'ショップが見つかりません');
		}
		$store = RktAccount::findOrFail($store_id);

		$redis = Redis::connection();
		$redis->set('rkt_accounts:'.$store->id.':ftp_account', $store->ftp_account);
		$redis->set('rkt_accounts:'.$store->id.':ftp_password', $store->ftp_password);
		$redis->set('rkt_accounts:'.$store->id.':css_type', $store->css_type);

		return View::make('malls.rakuten.make', compact('store'));
	}

	public function postNewStore()
	{
		$store_id = Input::get('store_id', null);
		if ( is_null($store_id) )
		{
			return Redirect::action('RakutenController@getIndex')->with('error', 'ショップが見つかりません');
		}

		$redis = Redis::connection();
		$ftp_account = $redis->get('rkt_accounts:'.$store_id.':ftp_account');
		$ftp_password = $redis->get('rkt_accounts:'.$store_id.':ftp_password');
		$css_type = $redis->get('rkt_accounts:'.$store_id.':css_type');
		// $unique_key = Hash::make(Auth::user()->email);
		$unique_key = Hash::make($ftp_account);
		$ftp = [
			'user' => $ftp_account,
			'password' => $ftp_password,
			'unique_key' => $unique_key,
			'id' => $store_id,
			'css' => $css_type
			];
		$result = $this->Unimall->ftpConnect($ftp, true);
		if ( $result['flg']==false)
		{
			return Redirect::action('RakutenController@getIndex')->with('error', $result['msg']);
		}
		// Queue::push('RakutenController@queueSaveItems', ['store_id', $store_id]);
		Queue::later(5, 'RakutenController@queueSaveItems', ['store_id', $store_id]);
		// Artisan::call('queue:work');

// 		$shop = RktAccount::findOrFail($store_id);
// 		// $shop = json_decode($shop);
// 		$shop_code = $shop->shop_code;
//
// $redis = Redis::connection();
// // $def_items = $redis->lindex("$shop_code:page1:default", 0);
// $default = $redis->get("$shop_code:page1:default");
// $asuraku = $redis->get("$shop_code:page1:asuraku");
// $review = $redis->get("$shop_code:page1:review");
// $point = $redis->get("$shop_code:page1:point");
// $sales = $redis->lrange("$shop_code:sales", 0, -1);
// $freeA = $redis->lrange("$shop_code:freeA", 0, -1);
// $freeB = $redis->lrange("$shop_code:freeB", 0, -1);
//
// dump_r($default);
// dump_r($asuraku);
// dump_r($review);
// dump_r($point);
// dump_r($sales);
// dump_r($freeA);
// dump_r($freeB);
//
// exit;

		return Redirect::action('RakutenController@getIndex')->with('success', '店舗を作成しています。通常、数分で登録したメールアドレスに完了通知が届きます。');

	}



// 	public function getFtp()
// 	{
// 		$items = $this->Unimall->searchShopCode('kyounokura', 'defaults', 'US');
// dump_r($items);exit;
// 		$this->Unimall->ftp();
// 		return 'ftp';
// 	}


	/**
	* ApiMethods
	* @note requestApiFunctions
	*/
	public function getApi($id, $trans_to = 'ja')
	{
		$redis = Redis::connection();
\Log::info('redis');
		$shop = RktAccount::find($id);
\Log::info($shop);
		$shop_code = $shop->shop_code;

		/* ptnC: with Redis */
		switch ($trans_to)
		{
			case 'ja':
				$area = 'NONE';
				break;
			case 'en':
				$area = 'US';
				break;
			case 'zh-CN':
				$area = 'CN';
				break;
			case 'zh-TW':
				$area = 'CN';
				break;
			case 'ko':
				$area = 'KR';
				break;
			default:
				$area = 'NONE';
				break;
		}
		// todo::Paginate
		// $contents['new'] = json_decode($redis->get("$shop_code:page1:default"), true);
		// $contents['popular'] = json_decode($redis->get("$shop_code:page1:review"), true);
		// $contents['sales'] = [];
		// $contents['freeA'] = [];
		// $contents['freeB'] = [];
		$newone = $redis->lrange("$shop_code:Items:newone", 0, 29);
		$popular = $redis->lrange("$shop_code:Items:review", 0, 29);
		$sales = $redis->lrange("$shop_code:Items:sales", 0, 29);
		$freeA = $redis->lrange("$shop_code:Items:freeA", 0, 3);
		$freeB = $redis->lrange("$shop_code:Items:freeB", 0, 29);
		$point = $redis->lrange("$shop_code:Items:point", 0, 29);
		$asuraku = $redis->lrange("$shop_code:Items:asuraku", 0, 29);

		$contents['new'] = ( !empty($newone) ) ? array_map(function($data_newone){
			return json_decode($data_newone, true);
		}, $newone) : [] ;
		$contents['popular'] = ( !empty($popular) ) ? array_map(function($data_popular){
			return json_decode($data_popular, true);
		}, $popular) : [] ;
		$contents['sales'] = ( !empty($sales) ) ? array_map(function($data_sales){
			return json_decode($data_sales, true);
		}, $sales) : [] ;
		$contents['freeA'] = ( !empty($freeA) ) ? array_map(function($data_A){
			return json_decode($data_A, true);
		}, $freeA) : [] ;
		$contents['freeB'] = ( !empty($freeB) ) ? array_map(function($data_B){
			return json_decode($data_B, true);
		}, $freeB) : [] ;
		$contents['point'] = ( !empty($point) ) ? array_map(function($data_point){
			return json_decode($data_point, true);
		}, $point) : [] ;
		$contents['asuraku'] = ( !empty($asuraku) ) ? array_map(function($data_asuraku){
			return json_decode($data_asuraku, true);
		}, $asuraku) : [] ;

		// translate
		$contents['statics'] = array(
			'title01' => '新着アイテム',
			'title02' => 'セール',
			'title03' => 'セール-2',
			'title04' => 'ポイントセール',
			'title05' => '送料無料アイテム',
			'title06' => '開催中のイベント',
			'sentence01' => '該当するアイテムがありません',
			'sentence02' => 'もっと見る',
			'sentence03' => 'カテゴリ一覧',
			'currency' => '円',
		);
		if ( $trans_to !== 'ja' )
		{
			$country = ['from' => 'ja', 'to' => $trans_to];
			array_walk($contents, function(&$content) use($country){
				if( in_array(key($content), ['new', 'popular', 'sales', 'freeA', 'freeB']) && is_array($content) ){
					foreach ($content as $key => &$data) {
						if ( !empty($data) && !is_null($data) ) {
							if (isset($data[0])) {
								$data[0]['itemName'] = Trans::translate($data[0]['itemName'], $country);
								$data[0]['itemCaption'] = Trans::translate($data[0]['itemCaption'], $country);
								$content[$key] = $data;
							}else{
								$data['itemName'] = Trans::translate($data['itemName'], $country);
								$data['itemCaption'] = Trans::translate($data['itemCaption'], $country);
								$content[$key] = $data;
							}
						}
					}
		// 			array_walk_recursive($content, function($data) use($country){
		// 				if ( !empty($data) && !is_null($data) ) {
		// 					$data = Trans::translate($data, $country);
		// Log::debug($data);
		// 				}
		// 			});
				}
			});
			$contents['statics'] = array(
				'title01' => Trans::translate('新着アイテム', $country),
				'title02' => Trans::translate('セール', $country),
				'title03' => Trans::translate('セール-2', $country),
				'title04' => Trans::translate('ポイントセール', $country),
				'title05' => Trans::translate('送料無料アイテム', $country),
				'title06' => Trans::translate('開催中のイベント', $country),
				'sentence01' => Trans::translate('該当するアイテムがありません', $country),
				'sentence02' => Trans::translate('もっと見る', $country),
				'sentence03' => Trans::translate('カテゴリ一覧', $country),
				'currency' => Trans::translate('円', $country),
			);
		}
\Log::info($contents['statics']);
		$view = View::make('elements/_items')->with('contents', $contents);
 // $data = ["content" => "$view"];
 // $data = $view;
		// $view = json_encode($view);
		// $response = Response::json($data);
// $response->headers->set('Access-Control-Allow-Origin', '*');
 // dump_r($response);exit;
// return $response;
// return Response::json( $data );
		return $view;

		/* ptnB: with API directs
		// Settings trans as area
		switch ($trans_to)
		{
			case 'ja':
				$area = 'NONE';
				break;
			case 'en':
				$area = 'US';
				break;
			case 'zh-CN':
				$area = 'CN';
				break;
			case 'zh-TW':
				$area = 'CN';
				break;
			case 'kr':
				$area = 'KR';
				break;
			default:
				$area = 'NONE';
				break;
		}

		$contents['new'] = $this->Unimall->searchShopCode('kyounokura', 'defaults', $area);
		$contents['popular'] = $this->Unimall->searchShopCode('kyounokura', 'review', $area);
		$contents['sales'] = [];
		$contents['freeA'] = [];
		$contents['freeB'] = [];


		// translate
		if ( $trans_to !== 'ja' )
		{
			$country = ['from' => 'ja', 'to' => $trans_to];

			foreach ($contents['new'] as &$content)
			{
				$content['itemName'] = Trans::translate($content['itemName'], $country);
				// $content['itemName'] = App::make('Trans')->translate($content['itemName'], $country);
			}
		}

		// $view = View::make('elements/items')->with('contents', $contents)->with('country', $country);
		$view = View::make('elements/items')->with('contents', $contents);

		return $view;
		*/

		/* ptnA: with DB
		$contents['new'] = RktContent::Arrival()->get();
		$contents['popular'] = RktContent::popular()->get();
		$contents['review'] = RktContent::review()->get();
		// trans_tmp
		$country = ['from' => 'ja', 'to' => $trans_to];
		foreach ($contents['new'] as $content)
		{
			$content->item_name = Trans::translate($content->item_name, $country);
		}
		$view = View::make('elements/items')->with('contents', $contents)->with('country', $country);
		return $view;
		*/
	}


	/**
	* QueueMethods
	* @note QueueFunctions
	* @note Redis - $shop_code:'page'.$page:$option or $area
	* @note Redis - xxx:'page' OR 'tmp'.$page:xxx (This is tmp for NewItems)
	*/
	public function queueSaveItems($job, $store_id)
	{
\Log::info('START_queue');
		$shop = RktAccount::findOrFail($store_id);
		$shop = json_decode($shop);
		$shop_code = $shop[0]->shop_code;
		$sales = $shop[0]->key_sales;
		$freeA = $shop[0]->key_freeA;
		$freeB = $shop[0]->key_freeB;
\Log::info($shop_code);

		$redis = Redis::connection();

		// Config指定最大ページ分取得
		for ($page=1; $page <= \Config::get('const.MAX_PAGE'); $page++)
		{
			// json形式でページ毎にredisにリスト化
			$default_items = $this->Unimall->searchShopCode($shop_code, $page);
\Log::info($default_items);
			if ( $default_items !== false )
			{
				$asuraku_items[] = $this->Unimall->searchShopCode($shop_code, $page, 'asuraku');
				$review_items[] = $this->Unimall->searchShopCode($shop_code, $page, 'review');
				$point_items[] = $this->Unimall->searchShopCode($shop_code, $page, 'point');

				$sales_items = array_filter($default_items, function($data) use ($sales){
					return strstr($data['catchcopy'], $sales) && !empty($data['catchcopy']) ;
				});
				$freeA_items = array_filter($default_items, function($data) use ($freeA){
					return strstr($data['itemName'], $freeA) && !empty($data['itemName']) ;
				});
				$freeB_items = array_filter($default_items, function($data) use ($freeB){
					return strstr($data['itemName'], $freeB) && !empty($data['itemName']) ;
				});

/** Ver.1.1 **/
				/* * 新着マスター更新 & 新着アイテム取得 * */
				// CAUNTION:: needs PHP5.5>= for array_column
				$newone = array_column($default_items, 'itemCode');
				$newone = array_unique($newone);
				$newdiff = $redis->sdiff("$shop_code:itemCode:tmp", "$shop_code:itemCode:master");

				$newone = array();
				foreach ($newdiff as $diff) {
					$newone[] = array_filter($default_items, function($data) use ($diff){
						return strstr($data['itemCode'], $diff) && !empty($data['itemName']);
					});
				}

				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:newone", json_encode($data));
				}, $newone);
				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:default", json_encode($data));
				}, $default_items);
				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:asuraku", json_encode($data));
				}, $asuraku_items);
				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:review", json_encode($data));
				}, $review_items);
				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:point", json_encode($data));
				}, $point_items);
				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:sales", json_encode($data));
				}, $sales_items);
				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:freeA", json_encode($data));
				}, $freeA_items);
				array_map(function($data) use ($redis, $shop_code){
					$redis->lpush("$shop_code:Items:freeB", json_encode($data));
				}, $freeB_items);


/** Ver.1.0
				/* * 差分=新着アイテム *
				$redis->sadd("$shop_code:tmp$page:default", json_encode($default_items));
				$newone = $redis->sdiff("$shop_code:tmp$page:default", "$shop_code:page$page:default");
\Log::info($newone);

				// $redis->set("$shop_code:page$page:default", json_encode($default_items));
				$redis->sadd("$shop_code:page$page:default", json_encode($default_items));
				// $redis->set("$shop_code:page$page:asuraku", json_encode($asuraku_items));
				$redis->sadd("$shop_code:page$page:asuraku", json_encode($asuraku_items));
				// $redis->set("$shop_code:page$page:review", json_encode($review_items));
				$redis->sadd("$shop_code:page$page:review", json_encode($review_items));
				// $redis->set("$shop_code:page$page:point", json_encode($point_items));
				$redis->sadd("$shop_code:page$page:point", json_encode($point_items));

				array_map(function($data) use ($redis, $shop_code){
					// $redis->lpush("$shop_code:sales", json_encode($data));
					$redis->lpush("$shop_code:sales", json_encode($data));
				}, $sales_items);
				array_map(function($data) use ($redis, $shop_code){
					// $redis->lpush("$shop_code:freeA", json_encode($data));
					$redis->lpush("$shop_code:freeA", json_encode($data));
				}, $freeA_items);
				array_map(function($data) use ($redis, $shop_code){
					// $redis->lpush("$shop_code:freeB", json_encode($data));
					$redis->lpush("$shop_code:freeB", json_encode($data));
				}, $freeB_items);
*/

			}
		}

\Log::info('queue_SUCCEED');
\Log::info('MailSend');

		// todo::宛先指定, URL確認
		// Mail::send('emails.completeQueue', ['url' => $shop[0]->shop_url. 'uni/ja/index.html'], function($message)
		// {
		// 	$message->to('sugihara.valis@icloud.com')->subject('店舗の作成が完了しました!!');
		// });

		$job->delete();

	}



	public function test($id, $trans_to='en')
	{
		$redis = Redis::connection();
\Log::info('redis');
		$shop = RktAccount::find($id);
\Log::info($shop);
		$shop_code = $shop->shop_code;

		/* ptnC: with Redis */
		switch ($trans_to)
		{
			case 'ja':
				$area = 'NONE';
				break;
			case 'en':
				$area = 'US';
				break;
			case 'zh-CN':
				$area = 'CN';
				break;
			case 'zh-TW':
				$area = 'CN';
				break;
			case 'ko':
				$area = 'KR';
				break;
			default:
				$area = 'NONE';
				break;
		}

		// todo::Paginate
		// $contents['new'] = json_decode($redis->get("$shop_code:page1:default"), true);
		// $contents['popular'] = json_decode($redis->get("$shop_code:page1:review"), true);
		// $contents['sales'] = [];
		// $contents['freeA'] = [];
		// $contents['freeB'] = [];
		$newone = $redis->lrange("$shop_code:Items:newone", 0, 29);
		$popular = $redis->lrange("$shop_code:Items:review", 0, 29);
		$sales = $redis->lrange("$shop_code:Items:sales", 0, 29);
		$freeA = $redis->lrange("$shop_code:Items:freeA", 0, 3);
		$freeB = $redis->lrange("$shop_code:Items:freeB", 0, 29);
		$point = $redis->lrange("$shop_code:Items:point", 0, 29);
		$asuraku = $redis->lrange("$shop_code:Items:asuraku", 0, 29);

		$contents['new'] = ( !empty($newone) ) ? array_map(function($data_newone){
			return json_decode($data_newone, true);
		}, $newone) : [] ;
		$contents['popular'] = ( !empty($popular) ) ? array_map(function($data_popular){
			return json_decode($data_popular, true);
		}, $popular) : [] ;
		$contents['sales'] = ( !empty($sales) ) ? array_map(function($data_sales){
			return json_decode($data_sales, true);
		}, $sales) : [] ;
		$contents['freeA'] = ( !empty($freeA) ) ? array_map(function($data_A){
			return json_decode($data_A, true);
		}, $freeA) : [] ;
		$contents['freeB'] = ( !empty($freeB) ) ? array_map(function($data_B){
			return json_decode($data_B, true);
		}, $freeB) : [] ;
		$contents['point'] = ( !empty($point) ) ? array_map(function($data_point){
			return json_decode($data_point, true);
		}, $point) : [] ;
		$contents['asuraku'] = ( !empty($asuraku) ) ? array_map(function($data_asuraku){
			return json_decode($data_asuraku, true);
		}, $asuraku) : [] ;

		// translate
		if ( $trans_to !== 'ja' )
		{
			$country = ['from' => 'ja', 'to' => $trans_to];
			array_walk($contents, function(&$content) use($country){
				if( in_array(key($content), ['new', 'popular', 'sales', 'freeA', 'freeB']) && is_array($content) ){
					foreach ($content as $key => &$data) {
						if ( !empty($data) && !is_null($data) ) {
							$data['itemName'] = Trans::translate($data['itemName'], $country);
							$data['itemCaption'] = Trans::translate($data['itemCaption'], $country);
							$content[$key] = $data;
						}
					}
		// 			array_walk_recursive($content, function($data) use($country){
		// 				if ( !empty($data) && !is_null($data) ) {
		// 					$data = Trans::translate($data, $country);
		// Log::debug($data);
		// 				}
		// 			});
				}
			});
		}
		$view = View::make('elements/_items')->with('contents', $contents);

		return $view;
	}


	// Test Function
	public function curl()
	{
		$rtn = $this->Unimall->curlLogin();
		return $rtn;
		return 'test';
	}

}
