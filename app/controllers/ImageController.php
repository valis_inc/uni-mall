<?php

class ImageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function display($store_id, $type)
	{
		$path = app_path(). '/images/' .$store_id. '/' .$type. '_banner.png';
		// header( "Content-Length: " . filesize( $path ) );
	    // header( "Content-type: image/png" );
		//
	    // readfile( $path );


		$image = Image::make(file_get_contents($path));
		// $top_banner = app_path(). '/images/' .$store_id. '/top_banner.png';
		// $bottom_banner = app_path(). '/images/' .$store_id. '/bottom_banner.png';

		$image->resize(320,200);

		// アスペクト比等倍
		// $image->resize(100, null, function ($constraint) {
		// 	$constraint->aspectRatio();
		// });
		// クロップ
		// $image->crop(100, 100);
		// 左上からクロップする
		// $image->crop(100, 100, 0, 0);
		// グレースケール
		// $image->greyscale();

		// $image->resize(100, null, function($constraint){
		// 	$constraint->aspectRatio();
		// })->crop(50,50,0,0)->greyscale();

		return $image->response('png');

	}


}
