<?php

class LoginController extends BaseController {
	protected $layout = 'layouts.master';

	public function getIndex()
	{
		return View::make('login.index');
	}

	// todo::validate
	public function postIndex()
	{
		$inputs = Input::all();
		if (Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password']], isset($inputs['remember_token'])))
		{
			// ユーザーの所有する店舗情報を取得
			$stores = User::findOrFail(Auth::id())->rakuten;
			Session::put('stores', $stores);
			return Redirect::intended('malls');
		}
		return Redirect::action('LoginController@getIndex')->with('error', 'ログインできませんでした');

	}

	public function anyLogout()
	{
		Session::flush();
		return Redirect::action('LoginController@getIndex');
	}


}
