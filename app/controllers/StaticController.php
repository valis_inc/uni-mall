<?php

class StaticController extends BaseController {
	protected $layout = 'layouts.master';

	public function getUsage()
	{
		return View::make('statics.usage');
	}

	public function getIndex()
	{
		return View::make('statics.index');
	}

	public function getPrice()
	{
		return View::make('statics.price');
	}

	public function getFaq()
	{
		return View::make('statics.faq');
	}

	public function getContact()
	{
		return View::make('statics.contact');
	}

}
