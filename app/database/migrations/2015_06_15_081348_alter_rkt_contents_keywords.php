<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRktContentsKeywords extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rkt_accounts', function($table)
		{
			$table->string('key_sales')->nullable()->after('genre_id');
			$table->string('key_freeA')->nullable()->after('key_sales');
			$table->string('key_freeB')->nullable()->after('key_freeA');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rkt_accounts', function($table)
		{
			$table->dropColumn('key_sales');
			$table->dropColumn('key_freeA');
			$table->dropColumn('key_freeB');
		});
	}

}
