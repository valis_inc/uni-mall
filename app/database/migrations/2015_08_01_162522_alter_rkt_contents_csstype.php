<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRktContentsCsstype extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rkt_accounts', function($table)
		{
			$table->string('css_type')->nullable()->after('genre_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rkt_accounts', function($table)
		{
			$table->dropColumn('css_type');
		});
	}

}
