<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table){
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
			$table->timestamp('deleted')->nullable();
		});
		Schema::create('rakutens', function($table){
			$table->increments('id');
			$table->integer('user_id');
			$table->string('shop_code')->nullable()->unique();
			$table->string('shop_name')->nullable();
			$table->string('shop_url')->nullable();
			$table->string('shop_affiliate_url')->nullable();
			$table->integer('genre_id')->nullable();
			$table->string('ftp_account')->nullable();
			$table->string('ftp_password')->nullable();
			$table->timestamps();
			$table->timestamp('deleted')->nullable();
		});
		Schema::create('ebays', function($table){
			$table->increments('id');
			$table->integer('user_id');
			$table->string('shop_code')->nullable()->unique();
			$table->string('shop_name')->nullable();
			$table->string('shop_url')->nullable();
			$table->string('shop_affiliate_url')->nullable();
			$table->integer('genre_id')->nullable();
			$table->string('ftp_account')->nullable();
			$table->string('ftp_password')->nullable();
			$table->timestamps();
			$table->timestamp('deleted')->nullable();
		});
		// Schema::create('accounts', function($table){
		// 	$table->increments('id');
		// 	$table->integer('user_id');
		// 	$table->string('rakuten_id')->nullable();
		// 	$table->string('rakuten_pass')->nullable();
		// 	$table->string('rakuten_ftp')->nullable();
		// 	$table->string('ebay_id')->nullable();
		// 	$table->string('ebay_pass')->nullable();
		// 	$table->string('ebay_ftp')->nullable();
		// 	$table->string('yahoo_id')->nullable();
		// 	$table->string('yahoo_pass')->nullable();
		// 	$table->string('yahoo_ftp')->nullable();
		// 	$table->timestamps();
		// 	$table->timestamp('deleted')->nullable();
		// });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('rakutens');
		Schema::dropIfExists('ebays');
		// Schema::drop('accounts');
		Schema::dropIfExists('rkt_contents');
		Schema::dropIfExists('rkt_accounts');
	}

}
