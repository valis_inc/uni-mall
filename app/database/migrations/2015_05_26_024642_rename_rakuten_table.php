<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRakutenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('rakutens'))
		{
			Schema::rename('rakutens', 'rkt_accounts');
		}

		Schema::create('rkt_contents', function($table)
		{
			$table->bigIncrements('id');
			$table->integer('accounts_id');
			$table->text('item_name');
			$table->string('item_url', 255);
			$table->tinyInteger('image_flag');
			$table->string('small_image_url_A', 255)->nullable();
			$table->string('small_image_url_B', 255)->nullable();
			$table->string('small_image_url_C', 255)->nullable();
			$table->string('medium_image_url_A', 255)->nullable();
			$table->string('medium_image_url_B', 255)->nullable();
			$table->string('medium_image_url_C', 255)->nullable();
			$table->tinyInteger('ship_overseas_flag');
			$table->string('ship_overseas_area');
			$table->integer('review_counts');
			$table->integer('review_average');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('accounts_id')->references('id')->on('rkt_accounts')
						->onDelete('cascade');
		});

		Schema::table('rkt_accounts', function($table)
		{
			// $table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')
						->onDeletes('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('rkt_contents');
		if (Schema::hasTable('rkt_accounts'))
		{
			Schema::rename('rkt_accounts', 'rakutens');
		}
		Schema::dropIfExists('rkt_accounts');
	}

}
