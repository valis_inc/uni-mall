$(function(){
/*=========================================================================
*=======================================================================*/

/*==============================
* button fade
*==============================*/

$(function(){
    $('.btn button,.btn a,.btn_block button,.btn_block a').on({
        'mouseenter':function(){
            $(this).addClass("active");
        },
        'mouseleave':function(){
            $(this).removeClass("active");
        }
    });
});

/*----------------------------------------------
     onLoad
----------------------------------------------*/
    $(window).load(function(){
        setupOverlay();
    });
/*==============================
* overlay
*==============================*/

function setupOverlay(){
    var $overlayBG = $('<div id="overlayBG"></div>').appendTo('body'),
        $overlay   = $('<div id="overlay"></div>').appendTo('body'),
        $inputs    = $('input, select, textarea'),
        $links     = $('a');
    $overlay.load('menu.html', setEvent);
    function setEvent(){
        $(document).on('click','.overlay_close,#overlayBG',hideOverlay);
        $('.btn_menu').on('click',showOverlay);
    }
    function showOverlay(){
        var bodyHeight = $('body').outerHeight();
        $overlayBG
        .height(bodyHeight)
        .fadeIn('normal');
        $overlay
        .fadeIn('normal');
        $('html,body').css({'overflow':'hidden'});
    }
    function hideOverlay(){
        $overlayBG.fadeOut('normal');
        $overlay.fadeOut('normal');
        $('html,body').clearStyle();
    }
}

/*----------------------------------------------
     footerHide
----------------------------------------------*/
$(function(){

    var footer= $('footer');

    $(function(){
      var scrollStopEvent = new $.Event("scrollstop");
      var delay = 1000;
      var timer;
     
      function scrollStopEventTrigger(){
        if (timer) {
          clearTimeout(timer);
        }
        timer = setTimeout(function(){$(window).trigger(scrollStopEvent)}, delay);
      }
      $(window).on("scroll", scrollStopEventTrigger);
      $("body").on("touchmove", scrollStopEventTrigger);
    });

    // $(function(){
    //     Shared.util.addResizeListener(function(w,h){
    //         footer.width($("#container").width());
    //     });
    // });

    $(function(){
        $(window).on("scroll",function(){
            // footer.stop().animate({bottom: -50},200,'oX2');
            if(Shared.css.hasTransition){
                footer.stop().transit({transform:'translate3d(0,50px,0)'}, 200, 'ioX2');
            }else{
                footer.stop().animate({bottom:-50},200,'oX2');
            }
        });
    });

    $(function(){
        $(window).on("scrollstop",function(){
            // footer.stop().animate({bottom: 0},300,'oX2');
            if(Shared.css.hasTransition){
                footer.stop().transit({transform:'translate3d(0,0,0)'}, 200, 'ioX2');
            }else{
                footer.stop().animate({bottom:0},200,'oX2');
            }
        });
    });

});

/*=========================================================================
*=======================================================================*/
});