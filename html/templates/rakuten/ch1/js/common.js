$(function(){
    $("#Hmenu").css("display","none");
    $(".button-toggle").on("click", function() {
        $("#Hmenu").slideToggle();
    });
});

$(function() {
    var headMenu = $('#headMenu');    
    headMenu.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            headMenu.fadeIn();
        } else {
            headMenu.fadeOut();
        }
    });
});

$(function() {
    var topBtn = $('#page-top');    
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});
