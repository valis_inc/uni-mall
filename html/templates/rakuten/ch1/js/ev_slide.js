/*----------------------------------------------
     slide
----------------------------------------------*/
$(function () {
    $('.ev_slide').slide({
        // 4秒ごとにスライド
        interval : 4000,
        // 操作したら止める
        stopIfManipulate : true,
        // ループあり
        loop : true,
        // サイズ調整
        resize : function(winW, winH){
            return $('.slide_block').width();
        },
        // 画面内に存在する場合のみキーボード操作有効
        keyboard : function(winW, winH){
            var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
            var offsetTop = $('.ev_slide').offset().top;
            if(scrollTop + winH < offsetTop || scrollTop > offsetTop + $('.ev_slide').height()){
                return false;
            }else{
                return true;
            }
        }
    });
});

